
/* ------------------------------------------------------------------------
!  When I was considering the initialization of the data abstraction process,
!  it was especially important for large portions of the code to be extensible
!  across all EHRs.  While it is easier to capture each ICD-10-CM code as it's 
!  own field and a seperate field for the POA flag, this greatly balloons the 
!  shared code for each of the subsequent screening, which would cause the 
!  case statement code to be thousands of lines (with no additional gains)
!  
!  Most RDBMS behind EHRs are able to streamline the aggregation process,
!  so the difference between individual fields and aggregation is minimal 
!  when the dataset is less than 1000 individuals.
!
!  There may be a performance gain by searching within a single aggregated
!  field vs searching each individual ICD-10_CM field.  This would offset any
!  small performance loss that is seen by aggregating the list of ICD-10-CM codes,
!  and it minimizes the code requirement for the search.
!------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------
!  These case statements are designed to be used on a aggregated ICD-10-CM, ICD-10-PCS,
!  and Medical History lists.  Here is an example of the 
!
!
!------------------------------------------------------------------------- */


, CC_MCCS_MAPPED AS (
SELECT
CS.PAT_ENC_CSN_ID

/* ------------------------------------------------------------------------
! CO-MORBIDITY EVALUATION - CURRENT ADMISSION AND MEDICAL HISTORY
!------------------------------------------------------------------------- */

, CASE
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%B20%'    OR CS.FINAL_DX_LIST LIKE '%B20%'       THEN 1  -- HIV disease resulting in infectious and parasitic diseases
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%B97.35%' OR CS.FINAL_DX_LIST LIKE '%B97.35%'    THEN 1  -- HIV 2 as the cause of diseases classified elsewhere
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%O98.7%'  OR CS.FINAL_DX_LIST LIKE '%O98.7%'     THEN 1  -- HIV disease complicating pregnancy, first trimester
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%Z21%'    OR CS.FINAL_DX_LIST LIKE '%Z21%'       THEN 1  -- Asymptomatic human immunodeficiency virus infection status
             ELSE 0 -- Evaluates for the PRESENCE OF HIV
  END AS HX_AIDS_HIV_DISEASE

, CASE
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%J45.%'     OR CS.FINAL_DX_LIST LIKE '%J45.%'      THEN 1  -- ASTHMA
             ELSE 0 -- Evaluates for the presence of ASTHMA
  END AS HX_ASTHMA

, CASE
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%A52.74%'   OR CS.FINAL_DX_LIST LIKE '%A52.74%'    THEN 1  -- Syphilis of liver and other viscera
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%B67.0%'    OR CS.FINAL_DX_LIST LIKE '%B67.0%'     THEN 1  -- Echinococcus granulosus infection of liver
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%B67.5%'    OR CS.FINAL_DX_LIST LIKE '%B67.5%'     THEN 1  -- Echinococcus multilocularis infection of liver
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K70%'      OR CS.FINAL_DX_LIST LIKE '%K70%'       THEN 1  -- Alcoholic liver disease 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K71%'      OR CS.FINAL_DX_LIST LIKE '%K71%'       THEN 1  -- Toxic liver disease 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K72%'      OR CS.FINAL_DX_LIST LIKE '%K72%'       THEN 1  -- Hepatic failure, not elsewhere classified 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K73%'      OR CS.FINAL_DX_LIST LIKE '%K73%'       THEN 1  -- Chronic hepatitis, not elsewhere classified
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K74%'      OR CS.FINAL_DX_LIST LIKE '%K74%'       THEN 1  -- Fibrosis and cirrhosis of liver 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K75%'      OR CS.FINAL_DX_LIST LIKE '%K75%'       THEN 1  -- Other inflammatory liver diseases 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K76%'      OR CS.FINAL_DX_LIST LIKE '%K76%'       THEN 1  -- Other diseases of liver 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K77%'      OR CS.FINAL_DX_LIST LIKE '%K77%'       THEN 1  -- Liver disorders in diseases classified elsewhere 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K91.82%'   OR CS.FINAL_DX_LIST LIKE '%K91.82%'    THEN 1  -- Postprocedural hepatic failure
             ELSE 0 -- Evaluates for the presence of CHRONIC LIVER DISEASE
  END AS HX_CHRONIC_LIVER_DISEASE

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D63.1%'    OR CS.FINAL_DX_LIST LIKE '%D63.1%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E08.22%'   OR CS.FINAL_DX_LIST LIKE '%E08.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E09.22%'   OR CS.FINAL_DX_LIST LIKE '%E09.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E10.22%'   OR CS.FINAL_DX_LIST LIKE '%E10.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E11.22%'   OR CS.FINAL_DX_LIST LIKE '%E11.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E13.22%'   OR CS.FINAL_DX_LIST LIKE '%E13.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.0%'    OR CS.FINAL_DX_LIST LIKE '%I12.0%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.9%'    OR CS.FINAL_DX_LIST LIKE '%I12.9%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13%'      OR CS.FINAL_DX_LIST LIKE '%I13%'       THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18%'      OR CS.FINAL_DX_LIST LIKE '%N18%'       THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.2%'    OR CS.FINAL_DX_LIST LIKE '%O10.2%'     THEN 1   -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.3%'    OR CS.FINAL_DX_LIST LIKE '%O10.3%'     THEN 1   -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%R88.0%'    OR CS.FINAL_DX_LIST LIKE '%R88.0%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z49.0%'    OR CS.FINAL_DX_LIST LIKE '%Z49.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z49.3%'    OR CS.FINAL_DX_LIST LIKE '%Z49.3%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of CHRONIC RENAL FAILURE
  END AS HX_CHRONIC_RENAL_DISEASE 

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J95.822%'  OR CS.FINAL_DX_LIST LIKE '%J95.822%'   THEN 1  -- Acute and chronic postprocedural respiratory failure
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J96.1%'    OR CS.FINAL_DX_LIST LIKE '%J96.1%'     THEN 1  -- Chronic respiratory failure, unsp w hypoxia or hypercapnia
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J96.2%'    OR CS.FINAL_DX_LIST LIKE '%J96.2%'     THEN 1  -- Acute and chr resp failure, unsp w hypoxia or hypercapnia
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z99.1%'    OR CS.FINAL_DX_LIST LIKE '%Z99.1%'     THEN 1  -- Dependence on respirator [ventilator] status
             ELSE 0 -- Evaluates for the presence of CHRONIC RESPIRATORY FAILURE
  END AS HX_CHRONIC_RESPIRATORY_FAILURE

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I09.81%'   OR CS.FINAL_DX_LIST LIKE '%I09.81%'    THEN 1  -- Rheumatic heart failure
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13.0%'    OR CS.FINAL_DX_LIST LIKE '%I13.0%'     THEN 1  -- Hypertensive heart and chronic kidney disease with heart failure and stage 1 through stage 4 chronic kidney disease, or unsp
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13.2%'    OR CS.FINAL_DX_LIST LIKE '%I13.2%'     THEN 1  -- Hypertensive heart and chronic kidney disease with heart failure and with stage 5 chronic kidney disease, or end stage renal
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I50%'      OR CS.FINAL_DX_LIST LIKE '%I50%'       THEN 1  -- Left ventricular failure, unspecified
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z95.81%'   OR CS.FINAL_DX_LIST LIKE '%Z95.81%'    THEN 1  -- Presence of heart assist device
             ELSE 0 -- Evaluates for the presence of CONGESTIVE HEART FAILURE
  END AS HX_CONGESTIVE_HEART_FAILURE

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J41%'      OR CS.FINAL_DX_LIST LIKE '%J41%'       THEN 1  -- Simple chronic bronchitis
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J42%'      OR CS.FINAL_DX_LIST LIKE '%J42%'       THEN 1  -- Unspecified chronic bronchitis
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J43%'      OR CS.FINAL_DX_LIST LIKE '%J43%'       THEN 1  -- pulmonary emphysema 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J44%'      OR CS.FINAL_DX_LIST LIKE '%J44%'       THEN 1  -- Chronic obstructive pulmon disease w acute lower resp infct
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J47%'      OR CS.FINAL_DX_LIST LIKE '%J47%'       THEN 1  -- Bronchiectasis with acute lower respiratory infection
             ELSE 0 -- Evaluates for the presence of COPD
  END AS HX_COPD

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F01.5%'    OR CS.FINAL_DX_LIST LIKE '%F01.5%'     THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F02.8%'    OR CS.FINAL_DX_LIST LIKE '%F02.8%'     THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F03.9%'    OR CS.FINAL_DX_LIST LIKE '%F03.9%'     THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%G30%'      OR CS.FINAL_DX_LIST LIKE '%G30%'       THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%G31%'      OR CS.FINAL_DX_LIST LIKE '%G31%'       THEN 1  -- 
             ELSE 0 -- Evaluates for the presence of DEMENTIA
  END AS HX_DEMENTIA

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E08.22%'   OR CS.FINAL_DX_LIST LIKE '%E08.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E08.9%'    OR CS.FINAL_DX_LIST LIKE '%E08.9%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E09.%'     OR CS.FINAL_DX_LIST LIKE '%E09.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E10.%'     OR CS.FINAL_DX_LIST LIKE '%E10.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E11.%'     OR CS.FINAL_DX_LIST LIKE '%E11.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E13.%'     OR CS.FINAL_DX_LIST LIKE '%E13.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E89.1%'    OR CS.FINAL_DX_LIST LIKE '%E89.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N25.1%'    OR CS.FINAL_DX_LIST LIKE '%N25.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.%'     OR CS.FINAL_DX_LIST LIKE '%O24.%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIABETES
  END AS HX_DIABETES

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I95.3%'    OR CS.FINAL_DX_LIST LIKE '%I.95.3%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%R88.0%'    OR CS.FINAL_DX_LIST LIKE '%R88.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.502%'  OR CS.FINAL_DX_LIST LIKE '%T81.502%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.512%'  OR CS.FINAL_DX_LIST LIKE '%T81.512%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.522%'  OR CS.FINAL_DX_LIST LIKE '%T81.522%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.532%'  OR CS.FINAL_DX_LIST LIKE '%T81.532%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.592%'  OR CS.FINAL_DX_LIST LIKE '%T81.592%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T82.4%'    OR CS.FINAL_DX_LIST LIKE '%T82.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.611%'  OR CS.FINAL_DX_LIST LIKE '%T85.611%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.621%'  OR CS.FINAL_DX_LIST LIKE '%T85.621%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.631%'  OR CS.FINAL_DX_LIST LIKE '%T85.631%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.691'   OR CS.FINAL_DX_LIST LIKE '%T85.691%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.71%'   OR CS.FINAL_DX_LIST LIKE '%T85.71%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Y62.2%'    OR CS.FINAL_DX_LIST LIKE '%Y62.2%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Y84.1%'    OR CS.FINAL_DX_LIST LIKE '%Y84.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z49%'      OR CS.FINAL_DX_LIST LIKE '%Z49%'       THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z91.15%'   OR CS.FINAL_DX_LIST LIKE '%Z91.15%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z99.2%'    OR CS.FINAL_DX_LIST LIKE '%Z99.2%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIALYSIS
  END AS HX_DIALYSIS

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%H35.031%'  OR CS.FINAL_DX_LIST LIKE '%H35.031%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%H35.032%'  OR CS.FINAL_DX_LIST LIKE '%H35.032%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%H35.033%'  OR CS.FINAL_DX_LIST LIKE '%H35.033%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I10.%'     OR CS.FINAL_DX_LIST LIKE '%I10.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I11.%'     OR CS.FINAL_DX_LIST LIKE '%I11.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.%'     OR CS.FINAL_DX_LIST LIKE '%I12.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13.%'     OR CS.FINAL_DX_LIST LIKE '%I13.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I15.%'     OR CS.FINAL_DX_LIST LIKE '%I15.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I16.%'     OR CS.FINAL_DX_LIST LIKE '%I16.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I67.4%'    OR CS.FINAL_DX_LIST LIKE '%I67.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I97.3%'    OR CS.FINAL_DX_LIST LIKE '%I97.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.%'     OR CS.FINAL_DX_LIST LIKE '%O10.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O11.%'     OR CS.FINAL_DX_LIST LIKE '%O11.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O16.%'     OR CS.FINAL_DX_LIST LIKE '%O16.%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of HYPERTENSION
  END AS HX_HYPERTENSION

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D80.%'     OR CS.FINAL_DX_LIST LIKE '%D80.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D81.%'     OR CS.FINAL_DX_LIST LIKE '%D81.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D82.%'     OR CS.FINAL_DX_LIST LIKE '%D82.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D83.%'     OR CS.FINAL_DX_LIST LIKE '%D83.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D84.%'     OR CS.FINAL_DX_LIST LIKE '%D84.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D86.%'     OR CS.FINAL_DX_LIST LIKE '%D86.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D89.%'     OR CS.FINAL_DX_LIST LIKE '%D89.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%K50.%'     OR CS.FINAL_DX_LIST LIKE '%K50.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%K51.%'     OR CS.FINAL_DX_LIST LIKE '%K51.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%L93.%'     OR CS.FINAL_DX_LIST LIKE '%L93.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M05.%'     OR CS.FINAL_DX_LIST LIKE '%M05.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M06.%'     OR CS.FINAL_DX_LIST LIKE '%M06.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M30.%'     OR CS.FINAL_DX_LIST LIKE '%M30.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M31.%'     OR CS.FINAL_DX_LIST LIKE '%M31.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M32.%'     OR CS.FINAL_DX_LIST LIKE '%M32.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M33.%'     OR CS.FINAL_DX_LIST LIKE '%M33.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M34.%'     OR CS.FINAL_DX_LIST LIKE '%M34.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M35.%'     OR CS.FINAL_DX_LIST LIKE '%M35.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M36.%'     OR CS.FINAL_DX_LIST LIKE '%M36.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M45.%'     OR CS.FINAL_DX_LIST LIKE '%M45.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M48.8%'    OR CS.FINAL_DX_LIST LIKE '%M48.8%'     THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M49.%'     OR CS.FINAL_DX_LIST LIKE '%M49.%'      THEN 1    --
             ELSE 0 -- Evaluates for the presence of IMMUNOCOMPROMISED
  END AS HX_IMMUNOCOMPROMISED

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C81.%'     OR CS.FINAL_DX_LIST LIKE '%C81.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C82.%'     OR CS.FINAL_DX_LIST LIKE '%C82.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C83.%'     OR CS.FINAL_DX_LIST LIKE '%C83.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.0%'    OR CS.FINAL_DX_LIST LIKE '%C84.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.1%'    OR CS.FINAL_DX_LIST LIKE '%C84.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.4%'    OR CS.FINAL_DX_LIST LIKE '%C84.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.6%'    OR CS.FINAL_DX_LIST LIKE '%C84.6%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.7%'    OR CS.FINAL_DX_LIST LIKE '%C84.7%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.A%'    OR CS.FINAL_DX_LIST LIKE '%C84.A%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.Z%'    OR CS.FINAL_DX_LIST LIKE '%C84.Z%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C86.%'     OR CS.FINAL_DX_LIST LIKE '%C86.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C88.%'     OR CS.FINAL_DX_LIST LIKE '%C88.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C90.%'     OR CS.FINAL_DX_LIST LIKE '%C90.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C91.%'     OR CS.FINAL_DX_LIST LIKE '%C91.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C92.%'     OR CS.FINAL_DX_LIST LIKE '%C92.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C93.%'     OR CS.FINAL_DX_LIST LIKE '%C93.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C94.%'     OR CS.FINAL_DX_LIST LIKE '%C94.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C95.%'     OR CS.FINAL_DX_LIST LIKE '%C95.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C96.%'     OR CS.FINAL_DX_LIST LIKE '%C96.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z85.6%'    OR CS.FINAL_DX_LIST LIKE '%Z85.6%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z85.71%'   OR CS.FINAL_DX_LIST LIKE '%Z85.71%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of LYMPHOMA LEUKEMIA MULTI MYELOMA
  END AS HX_LYMPHOMA_LEUKEMIA_MULTI_MYELOMA

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C77.%'     OR CS.FINAL_DX_LIST LIKE '%C77.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C78.%'     OR CS.FINAL_DX_LIST LIKE '%C78.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C79.%'     OR CS.FINAL_DX_LIST LIKE '%C79.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C7B.%'     OR CS.FINAL_DX_LIST LIKE '%C7B.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%R18.0%'    OR CS.FINAL_DX_LIST LIKE '%R18.0%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of METASTATIC CANCER
  END AS HX_METASTATIC_CANCER

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E66.%'     OR CS.FINAL_DX_LIST LIKE '%E66.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O99.2%'    OR CS.FINAL_DX_LIST LIKE '%O99.2%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z68.3%'    OR CS.FINAL_DX_LIST LIKE '%Z68.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z68.4%'    OR CS.FINAL_DX_LIST LIKE '%Z68.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z68.54%'   OR CS.FINAL_DX_LIST LIKE '%Z68.54%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of OBESITY
  END AS HX_OBESITY

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O13.%'     OR CS.FINAL_DX_LIST LIKE '%013.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.4%'    OR CS.FINAL_DX_LIST LIKE '%O24.4%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of PREGNANCY_COMORBIDITY
  END AS HX_PREGNANCY

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O03.37%'   OR CS.FINAL_DX_LIST LIKE '%O03.37%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O03.87%'   OR CS.FINAL_DX_LIST LIKE '%O03.87%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O04.87%'   OR CS.FINAL_DX_LIST LIKE '%O04.87%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O07.37%'   OR CS.FINAL_DX_LIST LIKE '%O07.37%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O08.82%'   OR CS.FINAL_DX_LIST LIKE '%O08.82%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O75.3%'    OR CS.FINAL_DX_LIST LIKE '%O75.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O85%'      OR CS.FINAL_DX_LIST LIKE '%O85%'       THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O86.04%'   OR CS.FINAL_DX_LIST LIKE '%O86.04%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O98.8%'    OR CS.FINAL_DX_LIST LIKE '%O98.8%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O98.9%'    OR CS.FINAL_DX_LIST LIKE '%O98.9%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of PREGNANCY_DURING_HOSPITALIZATION
  END AS HX_PREGNANCY_DURING_HOSPITALIZATION_SEPSIS

  , CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O98.5%'    OR CS.FINAL_DX_LIST LIKE '%O98.5%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of PREGNANCY_DURING_HOSPITALIZATION
  END AS HX_PREGNANCY_DURING_HOSPITALIZATION_COVID

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F17.2%'    OR CS.FINAL_DX_LIST LIKE '%F17.2%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O99.3%'    OR CS.FINAL_DX_LIST LIKE '%O99.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%U07%'      OR CS.FINAL_DX_LIST LIKE '%U07%'       THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of SMOKING_VAPING
  END AS HX_SMOKING_VAPING

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J95%'      OR CS.FINAL_DX_LIST LIKE '%J95%'       THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z43.0%'    OR CS.FINAL_DX_LIST LIKE '%Z43.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z93.0%'    OR CS.FINAL_DX_LIST LIKE '%Z93.0%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of TRACHEOSTOMY_ARRIVAL
  END AS HX_TRACHEOSTOMY_ARRIVAL

/* ------------------------------------------------------------------------
! SEVERITY OF ILLNESS - CURRENT ADMISSION
!------------------------------------------------------------------------- */

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%G45%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I21%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I22%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I60%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I61%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I63%'        THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACUTE CARDIOVASCULAR
  END AS CC_ACUTE_CARDIOVASCULAR

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%G93.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.0%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.20%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.242%'    THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.3%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R41.82%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ALTERED MENTAL STATUS
  END AS CC_ALTERED_MENTAL_STATUS

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%D65%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D66%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D68.%'       THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D69%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D75.82%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%M36.2%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%O72.3%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R79.1%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COAGULOPATHY
  END AS CC_COAGULOPATHY

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%I05%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I06%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I07%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I08%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I09%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I20%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I23%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I24%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I25%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I34%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I35%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I36%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I37%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I65%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I66%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I67%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I68%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I69%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.02%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.03%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.04%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.05%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.06%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.07%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.08%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.09%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I73.89%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I73.9%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.2%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.3%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.4%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.5%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.820%'    THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z98.62%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of OTHER CARDIOVASCULAR
  END AS CC_OTHER_CARDIOVASCULAR

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.2%'      THEN 1    --  
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z20.826%' OR CS.FINAL_DX_LIST LIKE '%Z20.828%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS CC_COVID_POSITIVE

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%J10%'        THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of INFLUENZA (KNOWN OR SUSPECTED)
  END AS CC_INFLUENZA_POSITIVE


/* ------------------------------------------------------------------------
! TREATMENT VARIABLES
!------------------------------------------------------------------------- */

, CASE 
       WHEN  PCS.PX_LIST LIKE '%SA1D00Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D60Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D70Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D80Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D90Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E1M39Z%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIALYSIS TREATMENT
  END AS T_DIALYSIS

, CASE 
       WHEN  PCS.PX_LIST LIKE '%5A15223%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1522F%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1522G%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1522H%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A15A2F%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A15A2G%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A15A2H%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ECMO TREATMENT
  END AS T_ECMO

, CASE 
       WHEN  PCS.PX_LIST LIKE '%09HN7BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09HN8BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09H13EZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09H17EZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09H18EZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CHY7BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CHY8BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CH57BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CH58BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0WHQ73Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0WHQ7YZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1935Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1945Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1955Z%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of MECHANICAL VENTILATION TREATMENT
  END AS T_MECHANICAL_VENTILATION

, CASE 
       WHEN  PCS.PX_LIST LIKE '%5A09357%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09358%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09457%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09458%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09557%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09558%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of NON-INVASIVE VENTILATION TREATMENT
  END AS T_NI_VENTILATION

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033E5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW043E5%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of REMDESIVIR TREATMENT
  END AS T_REMDESIVIR

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033G5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW043G5%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of Sarilumab TREATMENT
  END AS T_SARILUMAB

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033H5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW043H5%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of Tocilizumab TREATMENT
  END AS T_TOCILIZUMAB

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW13325%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW14325%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of CONVALESCENT PLASMA TREATMENT
  END AS T_CON_PLASMA

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW013F5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW033F5%'         THEN 1    --   
       WHEN  PCS.PX_LIST LIKE '%XW043F5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW0DXF5%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of NOVEL TREATMENTS
  END AS T_NOVEL_TREATMENTS


/* ------------------------------------------------------------------------
! CLINICAL VARIABLES
!------------------------------------------------------------------------- */


, CASE 
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.2%'      THEN 1    --  
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z20.826%' OR CS.FINAL_DX_LIST LIKE '%Z20.828%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS C_COVID_VIRUS
  
, CASE 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z20.828%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS C_COVID_EXPOSURE
    
, CASE 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A41.02%'  OR CS.FINAL_DX_LIST LIKE '%A41.02%'      THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A49.02%'  OR CS.FINAL_DX_LIST LIKE '%A49.02%'      THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J15.212%' OR CS.FINAL_DX_LIST LIKE '%J15.212%'     THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z16%'     OR CS.FINAL_DX_LIST LIKE '%Z16%'         THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z22.322%' OR CS.FINAL_DX_LIST LIKE '%Z22.322%'     THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z86.14%'  OR CS.FINAL_DX_LIST LIKE '%Z86.14%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS C_DRUG_RESISTANT_PATHOGEN
      
, CASE 
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N10%'     OR CS.FINAL_DX_LIST LIKE '%N10%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N11%'     OR CS.FINAL_DX_LIST LIKE '%N11%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N13.6%'   OR CS.FINAL_DX_LIST LIKE '%N13.6%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N15.1%'   OR CS.FINAL_DX_LIST LIKE '%N15.1%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N15.9%'   OR CS.FINAL_DX_LIST LIKE '%N15.9%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N16%'     OR CS.FINAL_DX_LIST LIKE '%N16%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N28.84%'  OR CS.FINAL_DX_LIST LIKE '%N28.84%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J0%'      OR CS.FINAL_DX_LIST LIKE '%J0%'          THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J1%'      OR CS.FINAL_DX_LIST LIKE '%J1%'          THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J20%'     OR CS.FINAL_DX_LIST LIKE '%J20%'         THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J44.0%'   OR CS.FINAL_DX_LIST LIKE '%J44.0%'       THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J44.1%'   OR CS.FINAL_DX_LIST LIKE '%J44.1%'       THEN 2    -- RESPIRATORY    BY DEFINITION, THIS ONE DOESNT INDICATE INFECTION PRESENT
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J49%'     OR CS.FINAL_DX_LIST LIKE '%J49%'         THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J69%'     OR CS.FINAL_DX_LIST LIKE '%J69%'         THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J85%'     OR CS.FINAL_DX_LIST LIKE '%J85%'         THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%J86%'     OR CS.FINAL_DX_LIST LIKE '%J86%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A02%'     OR CS.FINAL_DX_LIST LIKE '%A02%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A08%'     OR CS.FINAL_DX_LIST LIKE '%A08%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A09%'     OR CS.FINAL_DX_LIST LIKE '%A09%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A15%'     OR CS.FINAL_DX_LIST LIKE '%A15%'         THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A17.83%'  OR CS.FINAL_DX_LIST LIKE '%A17.83%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A17.9%'   OR CS.FINAL_DX_LIST LIKE '%A17.9%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A17%'     OR CS.FINAL_DX_LIST LIKE '%A17%'         THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.0%'   OR CS.FINAL_DX_LIST LIKE '%A18.0%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.1%'   OR CS.FINAL_DX_LIST LIKE '%A18.1%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.2%'   OR CS.FINAL_DX_LIST LIKE '%A18.2%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.0%'   OR CS.FINAL_DX_LIST LIKE '%A18.0%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.83%'  OR CS.FINAL_DX_LIST LIKE '%A18.83%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18%'     OR CS.FINAL_DX_LIST LIKE '%A18%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A31.0%'   OR CS.FINAL_DX_LIST LIKE '%A31.0%'       THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A31.1%'   OR CS.FINAL_DX_LIST LIKE '%A31.1%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A32.0%'   OR CS.FINAL_DX_LIST LIKE '%A32.0%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A32.1%'   OR CS.FINAL_DX_LIST LIKE '%A32.1%'       THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A36.3%'   OR CS.FINAL_DX_LIST LIKE '%A36.3%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A36%'     OR CS.FINAL_DX_LIST LIKE '%A36%'         THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A37%'     OR CS.FINAL_DX_LIST LIKE '%A37%'         THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A39%'     OR CS.FINAL_DX_LIST LIKE '%A39%'         THEN 5    -- CNS - MENINGITIS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.0%'   OR CS.FINAL_DX_LIST LIKE '%A18.0%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.0%'   OR CS.FINAL_DX_LIST LIKE '%A42.0%'       THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.1%'   OR CS.FINAL_DX_LIST LIKE '%A42.1%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.7%'   OR CS.FINAL_DX_LIST LIKE '%A42.7%'       THEN 8    -- SEPTICEMIA
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.82%'  OR CS.FINAL_DX_LIST LIKE '%A42.82%'      THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.81%'  OR CS.FINAL_DX_LIST LIKE '%A42.81%'      THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A43.0%'   OR CS.FINAL_DX_LIST LIKE '%A43.0%'       THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A43.1%'   OR CS.FINAL_DX_LIST LIKE '%A43.1%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A46%'     OR CS.FINAL_DX_LIST LIKE '%A46%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A48.1%'   OR CS.FINAL_DX_LIST LIKE '%A48.1%'       THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.04%'  OR CS.FINAL_DX_LIST LIKE '%A50.04%'      THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.06%'  OR CS.FINAL_DX_LIST LIKE '%A50.06%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.07%'  OR CS.FINAL_DX_LIST LIKE '%A50.07%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.4%'   OR CS.FINAL_DX_LIST LIKE '%A50.4%'       THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A51.3%'   OR CS.FINAL_DX_LIST LIKE '%A51.3%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A51.41%'  OR CS.FINAL_DX_LIST LIKE '%A51.41%'      THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.1%'   OR CS.FINAL_DX_LIST LIKE '%A52.1%'       THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.2%'   OR CS.FINAL_DX_LIST LIKE '%A52.2%'       THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.3%'   OR CS.FINAL_DX_LIST LIKE '%A52.3%'       THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.72%'  OR CS.FINAL_DX_LIST LIKE '%A52.72%'      THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.73%'  OR CS.FINAL_DX_LIST LIKE '%A52.73%'      THEN 2    -- RESPIRATORY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.85%'  OR CS.FINAL_DX_LIST LIKE '%A54.85%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.86%'  OR CS.FINAL_DX_LIST LIKE '%A54.86%'      THEN 8    -- SEPTICEMIA
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.89%'  OR CS.FINAL_DX_LIST LIKE '%A54.89%'      THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.8%'   OR CS.FINAL_DX_LIST LIKE '%A54.8%'       THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54%'     OR CS.FINAL_DX_LIST LIKE '%A54%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B78.1%'   OR CS.FINAL_DX_LIST LIKE '%B78.1%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%E83.2%'   OR CS.FINAL_DX_LIST LIKE '%E83.2%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%G0%'      OR CS.FINAL_DX_LIST LIKE '%G0%'          THEN 5    -- CNS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%I80%'     OR CS.FINAL_DX_LIST LIKE '%I80%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K12.2%'   OR CS.FINAL_DX_LIST LIKE '%K12.2%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K35%'     OR CS.FINAL_DX_LIST LIKE '%K35%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K36%'     OR CS.FINAL_DX_LIST LIKE '%K36%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K37%'     OR CS.FINAL_DX_LIST LIKE '%K37%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.014%' OR CS.FINAL_DX_LIST LIKE '%K50.014%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.114%' OR CS.FINAL_DX_LIST LIKE '%K50.114%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.814%' OR CS.FINAL_DX_LIST LIKE '%K50.814%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.914%' OR CS.FINAL_DX_LIST LIKE '%K50.914%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.014%' OR CS.FINAL_DX_LIST LIKE '%K51.014%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.214%' OR CS.FINAL_DX_LIST LIKE '%K51.214%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.314%' OR CS.FINAL_DX_LIST LIKE '%K51.314%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.414%' OR CS.FINAL_DX_LIST LIKE '%K51.414%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.514%' OR CS.FINAL_DX_LIST LIKE '%K51.514%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.814%' OR CS.FINAL_DX_LIST LIKE '%K51.814%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.914%' OR CS.FINAL_DX_LIST LIKE '%K51.914%'     THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.0%'   OR CS.FINAL_DX_LIST LIKE '%K57.0%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.12%'  OR CS.FINAL_DX_LIST LIKE '%K57.12%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.13%'  OR CS.FINAL_DX_LIST LIKE '%K57.13%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.2%'   OR CS.FINAL_DX_LIST LIKE '%K57.2%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.32%'  OR CS.FINAL_DX_LIST LIKE '%K57.32%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.33%'  OR CS.FINAL_DX_LIST LIKE '%K57.33%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.4%'   OR CS.FINAL_DX_LIST LIKE '%K57.4%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.52%'  OR CS.FINAL_DX_LIST LIKE '%K57.52%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.53%'  OR CS.FINAL_DX_LIST LIKE '%K57.53%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.8%'   OR CS.FINAL_DX_LIST LIKE '%K57.8%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.92%'  OR CS.FINAL_DX_LIST LIKE '%K57.92%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.93%'  OR CS.FINAL_DX_LIST LIKE '%K57.93%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K61%'     OR CS.FINAL_DX_LIST LIKE '%K61%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K63.0%'   OR CS.FINAL_DX_LIST LIKE '%K63.0%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K63.1%'   OR CS.FINAL_DX_LIST LIKE '%K63.1%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K65%'     OR CS.FINAL_DX_LIST LIKE '%K65%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K67%'     OR CS.FINAL_DX_LIST LIKE '%K67%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K68%'     OR CS.FINAL_DX_LIST LIKE '%K68%'         THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K75.0%'   OR CS.FINAL_DX_LIST LIKE '%K75.0%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K75.1%'   OR CS.FINAL_DX_LIST LIKE '%K75.1%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K81.0%'   OR CS.FINAL_DX_LIST LIKE '%K81.0%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K81.2%'   OR CS.FINAL_DX_LIST LIKE '%K81.2%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K82.2%'   OR CS.FINAL_DX_LIST LIKE '%K82.2%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K83.2%'   OR CS.FINAL_DX_LIST LIKE '%K83.2%'       THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.02%'  OR CS.FINAL_DX_LIST LIKE '%K85.02%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.12%'  OR CS.FINAL_DX_LIST LIKE '%K85.12%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.22%'  OR CS.FINAL_DX_LIST LIKE '%K85.22%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.32%'  OR CS.FINAL_DX_LIST LIKE '%K85.32%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.82%'  OR CS.FINAL_DX_LIST LIKE '%K85.82%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.92%'  OR CS.FINAL_DX_LIST LIKE '%K85.92%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K90.81%'  OR CS.FINAL_DX_LIST LIKE '%K90.81%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.02%'  OR CS.FINAL_DX_LIST LIKE '%K94.02%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.12%'  OR CS.FINAL_DX_LIST LIKE '%K94.12%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.22%'  OR CS.FINAL_DX_LIST LIKE '%K94.22%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.32%'  OR CS.FINAL_DX_LIST LIKE '%K94.32%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K95.01%'  OR CS.FINAL_DX_LIST LIKE '%K95.01%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%K95.81%'  OR CS.FINAL_DX_LIST LIKE '%K95.81%'      THEN 3    -- GASTROINTESTINAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.01%'  OR CS.FINAL_DX_LIST LIKE '%L02.01%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.11%'  OR CS.FINAL_DX_LIST LIKE '%L02.11%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.21%'  OR CS.FINAL_DX_LIST LIKE '%L02.21%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.31%'  OR CS.FINAL_DX_LIST LIKE '%L02.31%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.41%'  OR CS.FINAL_DX_LIST LIKE '%L02.41%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.51%'  OR CS.FINAL_DX_LIST LIKE '%L02.51%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.61%'  OR CS.FINAL_DX_LIST LIKE '%L02.61%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.81%'  OR CS.FINAL_DX_LIST LIKE '%L02.81%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.91%'  OR CS.FINAL_DX_LIST LIKE '%L02.91%'      THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L03%'     OR CS.FINAL_DX_LIST LIKE '%L03%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L04%'     OR CS.FINAL_DX_LIST LIKE '%L04%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L08%'     OR CS.FINAL_DX_LIST LIKE '%L08%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L88%'     OR CS.FINAL_DX_LIST LIKE '%L88%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L92.8%'   OR CS.FINAL_DX_LIST LIKE '%L92.8%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L98.0%'   OR CS.FINAL_DX_LIST LIKE '%L98.0%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%L98.3%'   OR CS.FINAL_DX_LIST LIKE '%L98.3%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M00%'     OR CS.FINAL_DX_LIST LIKE '%M00%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M01%'     OR CS.FINAL_DX_LIST LIKE '%M01%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M00%'     OR CS.FINAL_DX_LIST LIKE '%M00%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M46.2%'   OR CS.FINAL_DX_LIST LIKE '%M46.2%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M60%'     OR CS.FINAL_DX_LIST LIKE '%M60%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M71%'     OR CS.FINAL_DX_LIST LIKE '%M71%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M72.6%'   OR CS.FINAL_DX_LIST LIKE '%M72.6%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M72.8%'   OR CS.FINAL_DX_LIST LIKE '%M72.8%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M86%'     OR CS.FINAL_DX_LIST LIKE '%M86%'         THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M89.6%'   OR CS.FINAL_DX_LIST LIKE '%M89.6%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%M90.8%'   OR CS.FINAL_DX_LIST LIKE '%M90.8%'       THEN 4    -- SKIN/SOFT TISSUE
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N28.85%'  OR CS.FINAL_DX_LIST LIKE '%N28.85%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N28.86%'  OR CS.FINAL_DX_LIST LIKE '%N28.86%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N30%'     OR CS.FINAL_DX_LIST LIKE '%N30%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N34%'     OR CS.FINAL_DX_LIST LIKE '%N34%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N41%'     OR CS.FINAL_DX_LIST LIKE '%N41%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N43.1%'   OR CS.FINAL_DX_LIST LIKE '%N43.1%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N45%'     OR CS.FINAL_DX_LIST LIKE '%N45%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N48.2%'   OR CS.FINAL_DX_LIST LIKE '%N48.2%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N49%'     OR CS.FINAL_DX_LIST LIKE '%N49%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N51%'     OR CS.FINAL_DX_LIST LIKE '%N51%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N61.0%'   OR CS.FINAL_DX_LIST LIKE '%N61.0%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N61.1%'   OR CS.FINAL_DX_LIST LIKE '%N61.1%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N70%'     OR CS.FINAL_DX_LIST LIKE '%N70%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N71%'     OR CS.FINAL_DX_LIST LIKE '%N71%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N72%'     OR CS.FINAL_DX_LIST LIKE '%N72%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N73%'     OR CS.FINAL_DX_LIST LIKE '%N73%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N74%'     OR CS.FINAL_DX_LIST LIKE '%N74%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N75%'     OR CS.FINAL_DX_LIST LIKE '%N75%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N76%'     OR CS.FINAL_DX_LIST LIKE '%N76%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N77%'     OR CS.FINAL_DX_LIST LIKE '%N77%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%N98.0%'   OR CS.FINAL_DX_LIST LIKE '%N98.0%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.0%'   OR CS.FINAL_DX_LIST LIKE '%O03.0%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.37%'  OR CS.FINAL_DX_LIST LIKE '%O03.37%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.38%'  OR CS.FINAL_DX_LIST LIKE '%O03.38%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.5%'   OR CS.FINAL_DX_LIST LIKE '%O03.5%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.87%'  OR CS.FINAL_DX_LIST LIKE '%O03.87%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.88%'  OR CS.FINAL_DX_LIST LIKE '%O03.88%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O07.0%'   OR CS.FINAL_DX_LIST LIKE '%O07.0%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O07.37%'  OR CS.FINAL_DX_LIST LIKE '%O07.37%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O07.38%'  OR CS.FINAL_DX_LIST LIKE '%O07.38%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O04.5%'   OR CS.FINAL_DX_LIST LIKE '%O04.5%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O04.87%'  OR CS.FINAL_DX_LIST LIKE '%O04.87%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O04.88%'  OR CS.FINAL_DX_LIST LIKE '%O04.88%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O08.0%'   OR CS.FINAL_DX_LIST LIKE '%O08.0%'       THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O08.82%'  OR CS.FINAL_DX_LIST LIKE '%O08.82%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O08.83%'  OR CS.FINAL_DX_LIST LIKE '%O08.83%'      THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O23%'     OR CS.FINAL_DX_LIST LIKE '%O23%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O85%'     OR CS.FINAL_DX_LIST LIKE '%O85%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O86%'     OR CS.FINAL_DX_LIST LIKE '%O86%'         THEN 1    -- GENITOURINARY
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%O91%'     OR CS.FINAL_DX_LIST LIKE '%O91%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A00%'     OR CS.FINAL_DX_LIST LIKE '%A00%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A01%'     OR CS.FINAL_DX_LIST LIKE '%A01%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A04%'     OR CS.FINAL_DX_LIST LIKE '%A04%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A05%'     OR CS.FINAL_DX_LIST LIKE '%A05%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A19%'     OR CS.FINAL_DX_LIST LIKE '%A18%'         THEN 6    -- OTHER - BACTEREMIA
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A2%'      OR CS.FINAL_DX_LIST LIKE '%A2%'          THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A31%'     OR CS.FINAL_DX_LIST LIKE '%A31%'         THEN 4    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A32%'     OR CS.FINAL_DX_LIST LIKE '%A32%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A35%'     OR CS.FINAL_DX_LIST LIKE '%A35%'         THEN 6    -- OTHER - TETANUS
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A36.8%'   OR CS.FINAL_DX_LIST LIKE '%A36.8%'       THEN 6    -- OTHER - DIPHTHERIA
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A38%'     OR CS.FINAL_DX_LIST LIKE '%A38%'         THEN 6    -- OTHER - SCARLET FEVER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42%'     OR CS.FINAL_DX_LIST LIKE '%A42%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A43%'     OR CS.FINAL_DX_LIST LIKE '%A43%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A48%'     OR CS.FINAL_DX_LIST LIKE '%A48%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A49%'     OR CS.FINAL_DX_LIST LIKE '%A49%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50%'     OR CS.FINAL_DX_LIST LIKE '%A50%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A51%'     OR CS.FINAL_DX_LIST LIKE '%A51%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52%'     OR CS.FINAL_DX_LIST LIKE '%A52%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A53%'     OR CS.FINAL_DX_LIST LIKE '%A53%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A56%'     OR CS.FINAL_DX_LIST LIKE '%A56%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A6%'      OR CS.FINAL_DX_LIST LIKE '%A6%'          THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B35%'     OR CS.FINAL_DX_LIST LIKE '%B35%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B36%'     OR CS.FINAL_DX_LIST LIKE '%B36%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B95%'     OR CS.FINAL_DX_LIST LIKE '%B95%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B96%'     OR CS.FINAL_DX_LIST LIKE '%B96%'         THEN 9    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%H32%'     OR CS.FINAL_DX_LIST LIKE '%H32%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%I30%'     OR CS.FINAL_DX_LIST LIKE '%I30%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%I32%'     OR CS.FINAL_DX_LIST LIKE '%I32%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%I33%'     OR CS.FINAL_DX_LIST LIKE '%I33%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%I38%'     OR CS.FINAL_DX_LIST LIKE '%I38%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%I39%'     OR CS.FINAL_DX_LIST LIKE '%I39%'         THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%I40%'     OR CS.FINAL_DX_LIST LIKE '%I40%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T80.2%'   OR CS.FINAL_DX_LIST LIKE '%T80.2%'       THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T81.4%'   OR CS.FINAL_DX_LIST LIKE '%T81.4%'       THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T82.6XXA%'OR CS.FINAL_DX_LIST LIKE '%T82.6XXA%'    THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T82.7XXA%'OR CS.FINAL_DX_LIST LIKE '%T82.7XXA%'    THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T82.6XXA%'OR CS.FINAL_DX_LIST LIKE '%T82.6XXA%'    THEN 6    -- OTHER
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B37%'     OR CS.FINAL_DX_LIST LIKE '%B37%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B38%'     OR CS.FINAL_DX_LIST LIKE '%B38%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B39%'     OR CS.FINAL_DX_LIST LIKE '%B39%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B40%'     OR CS.FINAL_DX_LIST LIKE '%B40%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B41%'     OR CS.FINAL_DX_LIST LIKE '%B41%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B42%'     OR CS.FINAL_DX_LIST LIKE '%B42%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B43%'     OR CS.FINAL_DX_LIST LIKE '%B43%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B44%'     OR CS.FINAL_DX_LIST LIKE '%B44%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B45%'     OR CS.FINAL_DX_LIST LIKE '%B45%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B46%'     OR CS.FINAL_DX_LIST LIKE '%B46%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B47%'     OR CS.FINAL_DX_LIST LIKE '%B47%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B48%'     OR CS.FINAL_DX_LIST LIKE '%B48%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%B49%'     OR CS.FINAL_DX_LIST LIKE '%B49%'         THEN 9    -- FUNGAL
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A40%'     OR CS.FINAL_DX_LIST LIKE '%A40%'         THEN 8    -- SEPTICEMIA
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%A41%'     OR CS.FINAL_DX_LIST LIKE '%A41%'         THEN 8    -- SEPTICEMIA
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%R65%'     OR CS.FINAL_DX_LIST LIKE '%R65%'         THEN 8    -- SEPTICEMIA
	   WHEN  MHX.MEDICAL_HX_LIST LIKE '%R78.81%'  OR CS.FINAL_DX_LIST LIKE '%R78.81%'      THEN 8    -- SEPTICEMIA
             ELSE 0 -- Evaluates for the presence of SITE OF INFECTION
  END AS C_SITE_OF_INFECTION
      
	  
/* ------------------------------------------------------------------------
! CLINICAL OUTCOME VARIABLES
!------------------------------------------------------------------------- */

, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%I82.4%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%I26.0%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%I26.9%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%O08.2%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%O88.2%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%O88.3%'       THEN 1    -- PE
             ELSE 0 -- Evaluates for the presence of PE/DVT
  END AS O_PE_DVT_ACUTE

, CASE
       WHEN CS.FINAL_DX_LIST LIKE '%Z93%'         THEN 1    -- PE
       WHEN  PCS.PX_LIST LIKE '%0B110F4%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0B113F4%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0B114F4%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACUTE TRACHEOSTOMY
  END AS O_TRACHEOSTOMY

, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%I20%'         THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I21%'         THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I22%'         THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.110%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.700%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.710%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.720%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.730%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.750%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.760%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.790%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_CARDIAC_ACS
  
, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%I63%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_CARDIAC_CVA
  
, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%B33.22%'      THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%I40%'         THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%I41%'         THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%I51.4%'       THEN 1    --
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_CARDIAC_MYOCARDITIS

, CASE
       WHEN CS.FINAL_DX_LIST LIKE '%O98.513%'     THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%O98.519%'     THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%O98.52%'      THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%O98.53%'      THEN 1    --
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_PREGNANCY_VIRAL


FROM
ICD_10_CM_LIST CS 
LEFT JOIN MEDICAL_HX_LIST MHX ON CS.PAT_ENC_CSN_ID = MHX.PAT_ENC_CSN_ID
LEFT JOIN ICD_10_PCS_LIST PCS ON CS.PAT_ENC_CSN_ID = PCS.PAT_ENC_CSN_ID
)