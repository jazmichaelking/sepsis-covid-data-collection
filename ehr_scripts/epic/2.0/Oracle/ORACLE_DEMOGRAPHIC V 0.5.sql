/* ------------------------------------------------------------------------
! Name: NYSDOH Sepsis and COVID Data Initiative
! File Name:
!
! Description: NYSDOH is making the move from a manual abstraction process to an automated data pull process.
!              This automated process is to allow for 
!
!
!  Additional Info:  
!
!                     This is an initial foray into a non-manual abstraction process.  We will likely see 
!                     iterative changes to the data collected as this is rolled out across the state.
!
!
!   Date      Name            Description
!  --------  --------------  ---------------------------------------------
!  11/09/20  G. Briddick     JPR#  74846, LAST REVISION 11/24/2020
!------------------------------------------------------------------------- */

with BasePop as
(
SELECT distinct
      HSP.PAT_ENC_CSN_ID
    , HSP.HSP_ACCOUNT_ID
	, HSP.INPATIENT_DATA_ID                  -- Needed to merge F_VENT_EPISODES table
    , PAT.PAT_MRN_ID
    , PAT.PAT_NAME
    , PAT.PAT_LAST_NAME
    , PAT.PAT_FIRST_NAME
    , PAT.SSN
    , PAT.BIRTH_DATE
    , TO_NUMBER(PAT.MARITAL_STATUS_C) AS MARITAL_STATUS
	/* ------------------------------------------------------------------------
	!  Since the current NYSDOH data collection doesn't define gender identity,
	!  I adopted the PCORNet definitions for Gender Identity definitions.
	!-------------------------------------------------------------------------*/
    , CASE
           WHEN P4.GENDER_IDENTITY_C = 6   THEN 'DC'               -- Declines to answer
           WHEN P4.GENDER_IDENTITY_C = 1   THEN 'F'                -- Female
           WHEN P4.GENDER_IDENTITY_C = 2   THEN 'M'                -- Male
           WHEN P4.GENDER_IDENTITY_C = 5   THEN 'OT'               -- Other 
           WHEN P4.GENDER_IDENTITY_C = 100 THEN 'GQ'               -- Gender Queer
           WHEN P4.GENDER_IDENTITY_C = 3   THEN 'TF'               -- Transgender Female
           WHEN P4.GENDER_IDENTITY_C = 4   THEN 'TM'               -- Transgender Male
        --   WHEN P4.GENDER_IDENTITY_C IS NULL THEN NULL
         ELSE 'UN'
       END AS GENDER_IDENTITY
	/* ------------------------------------------------------------------------
	!  Since the current NYSDOH data collection doesn't define sex,
	!  I adopted the PCORNet definitions for sex definitions.
	!-------------------------------------------------------------------------*/

   , CASE
           WHEN SEX_C = 1   THEN 'F'                               -- Female
           WHEN SEX_C = 2   THEN 'M'                               -- Male
           WHEN SEX_C = 3   THEN 'UN'                              -- Unknown
           WHEN SEX_C = 999 THEN 'OT'                              -- Other
           WHEN SEX_C = 950 THEN 'A'                               -- Ambigious
           WHEN SEX_C = 951 THEN 'NI'                              -- No Information
         ELSE 'UN'                                                 -- Unknown
       END AS SEX

	/* ------------------------------------------------------------------------
	!  Since the current NYSDOH data collection doesn't define sexual orientation,
	!  I adopted the PCORNet definitions for sexual orientation definitions.
	!-------------------------------------------------------------------------*/

    , CASE 
           WHEN S.SEXUAL_ORIENTATN_C = 1 THEN 'LE'                 -- Asexual
           WHEN S.SEXUAL_ORIENTATN_C = 2 THEN 'ST'                 -- Straight
           WHEN S.SEXUAL_ORIENTATN_C = 3 THEN 'BI'                 -- Bisexual
           WHEN S.SEXUAL_ORIENTATN_C = 4 THEN 'SE'                 -- Something Else
           WHEN S.SEXUAL_ORIENTATN_C = 5 THEN 'UN'                 -- Unknown
           WHEN S.SEXUAL_ORIENTATN_C = 6 THEN 'DC'                 -- Declined
           WHEN S.SEXUAL_ORIENTATN_C = 7 THEN 'GA'                 -- Gay
           WHEN S.SEXUAL_ORIENTATN_C = 8 THEN 'LE'                 -- Lesbian
         ELSE 'UN'                                                 -- Unknown
       END AS SEXUAL_ORIENTATION
    , r.patient_race_c AS RACE
    , zcr.name AS RACE_DESC
    , PAT.ETHNIC_GROUP_C AS ETHNICITY
    , ZCE.NAME AS ETHNICITY_DESC

	/* ------------------------------------------------------------------------
	!  FIN-CLASS aligns with the Payer for SPARCS data collection.
	!  I have built out the categories which match SPARCS requirements
    !  The ACCT_FIN_CLASS_C that are triple digits are institution specific
    !  and are likely not universal for all Epic clients
	!-------------------------------------------------------------------------*/
    , CASE WHEN HA.ACCT_FIN_CLASS_C = 1   THEN 'F'                 -- 
           WHEN HA.ACCT_FIN_CLASS_C = 10  THEN 'F'
           WHEN HA.ACCT_FIN_CLASS_C = 100 THEN 'G'
           WHEN HA.ACCT_FIN_CLASS_C = 110 THEN 'L'
           WHEN HA.ACCT_FIN_CLASS_C = 120 THEN 'C'
           WHEN HA.ACCT_FIN_CLASS_C = 2   THEN 'C'
           WHEN HA.ACCT_FIN_CLASS_C = 3   THEN 'D'
           WHEN HA.ACCT_FIN_CLASS_C = 4   THEN 'A'
           WHEN HA.ACCT_FIN_CLASS_C = 5   THEN 'B' 
           WHEN HA.ACCT_FIN_CLASS_C = 130 THEN 'E'
           WHEN HA.ACCT_FIN_CLASS_C = 140 THEN 'D'
           WHEN HA.ACCT_FIN_CLASS_C = 150 THEN 'F'
           WHEN HA.ACCT_FIN_CLASS_C = 6   THEN 'H'
           WHEN HA.ACCT_FIN_CLASS_C = 7   THEN 'H'  
         ELSE HA.ACCT_FIN_CLASS_C
       END AS PAYER
    , MEM_NUMBER AS INSURANCE_NUMBER
    , ZCF.NAME AS FINANCIAL_CLASS_DESC
    , ZCS.NAME AS SOURCE_OF_ADMISSION
    , HA.NAME AS SOURCE_FACILITY
    , HSP.HOSP_ADMSN_TIME
	/* ------------------------------------------------------------------------
	!  REV_LOC_ID is to be matched to the FACILITY_IDENTIFIER/PFI for the institution.
	!  For Upstate's two campuses, these are 635/628, and will need to be substituted
	!  for institutions adopting this code.
	!-------------------------------------------------------------------------*/
    , CASE 
           WHEN DEP.REV_LOC_ID = 111001 THEN 635
           ELSE CASE WHEN DEP.REV_LOC_ID = 111010 THEN 628 END 
       END AS FACILITY_IDENTIFIER
    , HSP.HOSP_DISCH_TIME
    , HSP.DISCH_DISP_C
    , FIP.ADM_PROV_ID
    , FIP.ADM_ATND_PROV_ID
    , FIP.DISCH_PROV_ID
    , DISCH_ATND_PROV_ID
    , trunc(round((HSP.CONTACT_DATE-PAT.BIRTH_DATE)/365.25, 5)) AS AGE
    , ROW_NUMBER() OVER (PARTITION BY HSP.PAT_ENC_CSN_ID ORDER BY HSP.PAT_ENC_CSN_ID) ADM_NUM
FROM PAT_ENC_HSP HSP
     INNER JOIN Clarity.ZZ_IP_SEPSIS_IV_ATB        ATB ON HSP.PAT_ENC_CSN_ID = ATB.PAT_ENC_CSN_ID and atb.med_order = 1
     INNER JOIN PATIENT                            PAT ON HSP.PAT_ID = PAT.PAT_ID
     INNER JOIN CLARITY_DEP                        DEP ON HSP.DEPARTMENT_ID = DEP.DEPARTMENT_ID
     INNER JOIN PATIENT_RACE                         R ON PAT.PAT_ID = R.PAT_ID
     INNER JOIN ZC_PATIENT_RACE                    ZCR ON R.PATIENT_RACE_C = ZCR.PATIENT_RACE_C
     INNER JOIN ZC_ETHNIC_GROUP                    ZCE ON PAT.ETHNIC_GROUP_C = ZCE.ETHNIC_GROUP_C
     INNER JOIN PATIENT_3                           P3 ON PAT.PAT_ID = P3.PAT_ID
     INNER JOIN HSP_ACCOUNT                         HA ON HSP.HSP_ACCOUNT_ID = HA.HSP_ACCOUNT_ID
     LEFT OUTER JOIN ZC_FIN_CLASS                  ZCF ON HA.ACCT_FIN_CLASS_C = ZCF.FIN_CLASS_C     
     LEFT OUTER JOIN V_COVERAGE_PAYOR_PLAN           P ON HA.COVERAGE_ID = P.COVERAGE_ID
     LEFT JOIN COVERAGE_MEM_LIST                     C ON HA.COVERAGE_ID = C.COVERAGE_ID
     INNER JOIN F_IP_HSP_ADMISSION                 FIP ON HSP.PAT_ENC_CSN_ID = FIP.PAT_ENC_CSN_ID
     INNER JOIN ZC_ADM_SOURCE                      ZCS ON FIP.POINT_OF_ORIGIN_C = ZCS.ADMIT_SOURCE_C
     LEFT JOIN ZC_TRANSFER_SRC_HA                   HA ON HSP.TRANSFER_FROM_C = HA.TRANSFER_SRC_HA_C
     INNER JOIN PATIENT_2                           P2 ON PAT.PAT_ID = P2.PAT_ID
     INNER JOIN PATIENT_4                           P4 ON PAT.PAT_ID = P4.PAT_ID
     LEFT OUTER JOIN ZC_GENDER_IDENTITY              G ON P4.GENDER_IDENTITY_C = G.GENDER_IDENTITY_C
     LEFT JOIN ZC_MARITAL_STATUS                     M ON PAT.MARITAL_STATUS_C = M.MARITAL_STATUS_C
     LEFT JOIN PAT_SEXUAL_ORIENTATION                S ON PAT.PAT_ID = S.PAT_ID AND S.LINE = 1
     LEFT JOIN ZC_SEXUAL_ORIENTATION                SO ON S.SEXUAL_ORIENTATN_C = SO.SEXUAL_ORIENTATION_C
     LEFT JOIN ZC_SEX                              SEX ON PAT.SEX_C = RCPT_MEM_SEX_C

where 
		(trunc(HSP.hosp_disch_time,'MM') between ADD_MONTHS(trunc(SYSDATE,'MM'),-4)  and LAST_DAY(ADD_MONTHS(Trunc(SYSDATE,'MM'),-1)+1))

)


/* ------------------------------------------------------------------------
! ICD-10-CM CODES
!------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------
!  When I was considering the initialization of the data abstraction process,
!  it was especially important for large portions of the code to be extensible
!  across all EHRs.  While it is easier to capture each ICD-10-CM code as it's 
!  own field and a seperate field for the POA flag, this greatly balloons the 
!  shared code for each of the subsequent screening, which would cause the 
!  case statement code to be thousands of lines (with no additional gains)
!  
!  Most RDBMS behind EHRs are able to streamline the aggregation process,
!  so the difference between individual fields and aggregation is minimal 
!  when the dataset is less than 1000 individuals.
!
!  There may be a performance gain by searching within a single aggregated
!  field vs searching each individual ICD-10_CM field.  This would offset any
!  small performance loss that is seen by aggregating the list of ICD-10-CM codes,
!  and it minimizes the code requirement for the search.
!------------------------------------------------------------------------- */

, ICD_10_CM_LIST AS (
 SELECT
             hsp.PAT_ENC_CSN_ID
	   , LISTAGG(EDG.DX_NAME || '-' || DX.FINAL_DX_POA_C, ', ') WITHIN GROUP (ORDER BY DX.LINE) AS FINAL_DX_NAMES
	   , LISTAGG(EDG.CURRENT_ICD10_LIST || '-' || DX.FINAL_DX_POA_C, ', ') WITHIN GROUP (ORDER BY DX.LINE) AS FINAL_DX_LIST

       FROM (
                SELECT DISTINCT
                      H.PAT_ENC_CSN_ID
                      ,H.HSP_ACCOUNT_ID
                FROM
                      pat_enc_hsp h 
                      INNER JOIN BASEPOP BP ON H.PAT_ENC_CSN_ID = BP.PAT_ENC_CSN_ID
                      INNER JOIN HSP_ACCT_DX_LIST DX ON h.HSP_ACCOUNT_ID = DX.HSP_ACCOUNT_ID
                      INNER JOIN CLARITY_EDG     E ON DX.DX_ID = E.DX_ID

                WHERE 

		(E.CURRENT_ICD10_LIST LIKE '%R65.2%' OR E.CURRENT_ICD10_LIST LIKE '%T81.12XA%')  -- SEPSIS ICD-10-CM CODES

		OR

		(E.CURRENT_ICD10_LIST LIKE '%U07.1%' OR E.CURRENT_ICD10_LIST LIKE '%U07.2%' OR E.CURRENT_ICD10_LIST LIKE '%B34.2%') -- COVID ICD-10-CM CODES

              )HSP

     INNER JOIN HSP_ACCT_DX_LIST DX ON hsp.HSP_ACCOUNT_ID = DX.HSP_ACCOUNT_ID
     INNER JOIN CLARITY_EDG     EDG ON DX.DX_ID = EDG.DX_ID
                 GROUP BY HSP.PAT_ENC_CSN_ID

)

/* ------------------------------------------------------------------------
! ICD-10-PCS CODES
!------------------------------------------------------------------------- */

, ICD_10_PCS_LIST AS (
 SELECT
             hsp.PAT_ENC_CSN_ID
	   -- , LISTAGG(E.PROCEDURE_NAME || ' - ' || TO_CHAR(PX.PROC_DATE, 'YYYY/MM/DD'), ', ') WITHIN GROUP (ORDER BY PX.LINE) AS PX_NAMES
	   , LISTAGG(E.REF_BILL_CODE || ' - ' || TO_CHAR(PX.PROC_DATE, 'YYYY/MM/DD'), ', ') WITHIN GROUP (ORDER BY PX.LINE) AS PX_LIST

       FROM (
                SELECT DISTINCT
                      H.PAT_ENC_CSN_ID
                      ,H.HSP_ACCOUNT_ID
                FROM
                      pat_enc_hsp h 
                      INNER JOIN BASEPOP BP ON H.PAT_ENC_CSN_ID = BP.PAT_ENC_CSN_ID
                      INNER JOIN HSP_ACCT_PX_LIST PX ON h.HSP_ACCOUNT_ID = PX.HSP_ACCOUNT_ID
                      INNER JOIN CL_ICD_PX     E ON PX.FINAL_ICD_PX_ID = E.ICD_PX_ID
)HSP

     INNER JOIN HSP_ACCT_PX_LIST PX ON hsp.HSP_ACCOUNT_ID = PX.HSP_ACCOUNT_ID
     INNER JOIN CL_ICD_PX     E ON PX.FINAL_ICD_PX_ID = E.ICD_PX_ID
                 GROUP BY HSP.PAT_ENC_CSN_ID
)

/* ------------------------------------------------------------------------
! CPT CODES
!------------------------------------------------------------------------- */

, CPT_LIST AS (
 SELECT
             hsp.PAT_ENC_CSN_ID
	   -- , LISTAGG(E.PROCEDURE_NAME || ', ' || PX.PROC_DATE, '; ') WITHIN GROUP (ORDER BY PX.LINE) AS PX_NAMES
	   , LISTAGG(CPT_CODE || ', ' || CPT_QUANTITY || ', ' || CPT_CODE_DATE, '; ') WITHIN GROUP (ORDER BY LINE) AS CPT_LIST

       FROM (
                SELECT DISTINCT
                      H.PAT_ENC_CSN_ID
                      ,H.HSP_ACCOUNT_ID
                FROM
                      pat_enc_hsp h 
                      INNER JOIN BASEPOP BP ON H.PAT_ENC_CSN_ID = BP.PAT_ENC_CSN_ID
                      INNER JOIN HSP_ACCT_CPT_CODES CPT ON h.HSP_ACCOUNT_ID = CPT.HSP_ACCOUNT_ID
)HSP

     INNER JOIN HSP_ACCT_CPT_CODES CPT ON hsp.HSP_ACCOUNT_ID = CPT.HSP_ACCOUNT_ID
                 GROUP BY HSP.PAT_ENC_CSN_ID
)

/* ------------------------------------------------------------------------
! MEDICAL HISTORY
!
!  Unfortunately, I am having difficulty with the Medical History pairing.
!  Since the PAT_ENC_CSN_ID for the medical history doesn't match the adm
!  PAT_ENC_CSN_ID, and I haven't invested the time to route the pairing. 
!  There can be multiple different PAT_ENC_CSN_IDs with medical history,
!  depending on if you are looking for the ED gathered medical history or
!  the admission provider's documented medical history.
!
! 
!------------------------------------------------------------------------- */

, MEDICAL_HX_LIST AS (
 SELECT
             h.PAT_ENC_CSN_ID
	   , LISTAGG(HX.CURRENT_ICD10_LIST, ', ') WITHIN GROUP (ORDER BY HX.CURRENT_ICD10_LIST) AS MEDICAL_HX_LIST 
              FROM (SELECT DISTINCT PAT_ENC_CSN_ID, CURRENT_ICD10_LIST FROM MEDICAL_HX HX_LIST INNER JOIN CLARITY_EDG EDG ON HX_LIST.DX_ID = EDG.DX_ID
		)HX
	     	INNER JOIN pat_enc_hsp h ON HX.PAT_ENC_CSN_ID = H.PAT_ENC_CSN_ID
                      INNER JOIN BASEPOP BP ON H.PAT_ENC_CSN_ID = BP.PAT_ENC_CSN_ID

                 GROUP BY H.PAT_ENC_CSN_ID

)

/* ------------------------------------------------------------------------
! MAP MEDICAL HISTORY/ICD-10-CS/ICD-10-PCS CODES
!------------------------------------------------------------------------- */

, CC_MCCS_MAPPED AS (
SELECT
CS.PAT_ENC_CSN_ID

/* ------------------------------------------------------------------------
! CO-MORBIDITY EVALUATION - CURRENT ADMISSION AND MEDICAL HISTORY
!------------------------------------------------------------------------- */

, CASE
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%B20%'    OR CS.FINAL_DX_LIST LIKE '%B20%'       THEN 1  -- HIV disease resulting in infectious and parasitic diseases
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%B97.35%' OR CS.FINAL_DX_LIST LIKE '%B97.35%'    THEN 1  -- HIV 2 as the cause of diseases classified elsewhere
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%O98.7%'  OR CS.FINAL_DX_LIST LIKE '%O98.7%'     THEN 1  -- HIV disease complicating pregnancy, first trimester
       WHEN   MHX.MEDICAL_HX_LIST  LIKE '%Z21%'    OR CS.FINAL_DX_LIST LIKE '%Z21%'       THEN 1  -- Asymptomatic human immunodeficiency virus infection status
             ELSE 0 -- Evaluates for the PRESENCE OF HIV
  END AS HX_AIDS_HIV_DISEASE

, CASE
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%J45.%'     OR CS.FINAL_DX_LIST LIKE '%J45.%'      THEN 1  -- ASTHMA
             ELSE 0 -- Evaluates for the presence of ASTHMA
  END AS HX_ASTHMA

, CASE
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%A52.74%'   OR CS.FINAL_DX_LIST LIKE '%A52.74%'    THEN 1  -- Syphilis of liver and other viscera
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%B67.0%'    OR CS.FINAL_DX_LIST LIKE '%B67.0%'     THEN 1  -- Echinococcus granulosus infection of liver
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%B67.5%'    OR CS.FINAL_DX_LIST LIKE '%B67.5%'     THEN 1  -- Echinococcus multilocularis infection of liver
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K70%'      OR CS.FINAL_DX_LIST LIKE '%K70%'       THEN 1  -- Alcoholic liver disease 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K71%'      OR CS.FINAL_DX_LIST LIKE '%K71%'       THEN 1  -- Toxic liver disease 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K72%'      OR CS.FINAL_DX_LIST LIKE '%K72%'       THEN 1  -- Hepatic failure, not elsewhere classified 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K73%'      OR CS.FINAL_DX_LIST LIKE '%K73%'       THEN 1  -- Chronic hepatitis, not elsewhere classified
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K74%'      OR CS.FINAL_DX_LIST LIKE '%K74%'       THEN 1  -- Fibrosis and cirrhosis of liver 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K75%'      OR CS.FINAL_DX_LIST LIKE '%K75%'       THEN 1  -- Other inflammatory liver diseases 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K76%'      OR CS.FINAL_DX_LIST LIKE '%K76%'       THEN 1  -- Other diseases of liver 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K77%'      OR CS.FINAL_DX_LIST LIKE '%K77%'       THEN 1  -- Liver disorders in diseases classified elsewhere 
       WHEN MHX.MEDICAL_HX_LIST  LIKE '%K91.82%'   OR CS.FINAL_DX_LIST LIKE '%K91.82%'    THEN 1  -- Postprocedural hepatic failure
             ELSE 0 -- Evaluates for the presence of CHRONIC LIVER DISEASE
  END AS HX_CHRONIC_LIVER_DISEASE

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D63.1%'    OR CS.FINAL_DX_LIST LIKE '%D63.1%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E08.22%'   OR CS.FINAL_DX_LIST LIKE '%E08.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E09.22%'   OR CS.FINAL_DX_LIST LIKE '%E09.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E10.22%'   OR CS.FINAL_DX_LIST LIKE '%E10.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E11.22%'   OR CS.FINAL_DX_LIST LIKE '%E11.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E13.22%'   OR CS.FINAL_DX_LIST LIKE '%E13.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.0%'    OR CS.FINAL_DX_LIST LIKE '%I12.0%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.9%'    OR CS.FINAL_DX_LIST LIKE '%I12.9%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13%'      OR CS.FINAL_DX_LIST LIKE '%I13%'       THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18%'      OR CS.FINAL_DX_LIST LIKE '%N18%'       THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.2%'    OR CS.FINAL_DX_LIST LIKE '%O10.2%'     THEN 1   -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.3%'    OR CS.FINAL_DX_LIST LIKE '%O10.3%'     THEN 1   -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%R88.0%'    OR CS.FINAL_DX_LIST LIKE '%R88.0%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z49.0%'    OR CS.FINAL_DX_LIST LIKE '%Z49.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z49.3%'    OR CS.FINAL_DX_LIST LIKE '%Z49.3%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of CHRONIC RENAL FAILURE
  END AS HX_CHRONIC_RENAL_DISEASE 
  
, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z49%'      OR CS.FINAL_DX_LIST LIKE '%Z49%'       THEN 6    --  
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18.1%'    OR CS.FINAL_DX_LIST LIKE '%N18.1%'     THEN 1     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18.2%'    OR CS.FINAL_DX_LIST LIKE '%N18.2%'     THEN 2     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18.3%'    OR CS.FINAL_DX_LIST LIKE '%N18.3%'     THEN 3     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18.4%'    OR CS.FINAL_DX_LIST LIKE '%N18.4%'     THEN 4     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18.5%'    OR CS.FINAL_DX_LIST LIKE '%N18.5%'     THEN 5     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18.6%'    OR CS.FINAL_DX_LIST LIKE '%N18.6%'     THEN 6     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N18.9%'    OR CS.FINAL_DX_LIST LIKE '%N18.9%'     THEN 9     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.2%'    OR CS.FINAL_DX_LIST LIKE '%O10.2%'     THEN 9   -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.3%'    OR CS.FINAL_DX_LIST LIKE '%O10.3%'     THEN 9   -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%R88.0%'    OR CS.FINAL_DX_LIST LIKE '%R88.0%'     THEN 9     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D63.1%'    OR CS.FINAL_DX_LIST LIKE '%D63.1%'     THEN 9     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E08.22%'   OR CS.FINAL_DX_LIST LIKE '%E08.22%'    THEN 9    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E09.22%'   OR CS.FINAL_DX_LIST LIKE '%E09.22%'    THEN 9    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E10.22%'   OR CS.FINAL_DX_LIST LIKE '%E10.22%'    THEN 9    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E11.22%'   OR CS.FINAL_DX_LIST LIKE '%E11.22%'    THEN 9    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E13.22%'   OR CS.FINAL_DX_LIST LIKE '%E13.22%'    THEN 9    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.0%'    OR CS.FINAL_DX_LIST LIKE '%I12.0%'     THEN 9     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.9%'    OR CS.FINAL_DX_LIST LIKE '%I12.9%'     THEN 9     -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13%'      OR CS.FINAL_DX_LIST LIKE '%I13%'       THEN 9     -- 
             ELSE 0 -- Evaluates for the presence of CHRONIC RENAL FAILURE
  END AS HX_CKD_STAGE

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J95.822%'  OR CS.FINAL_DX_LIST LIKE '%J95.822%'   THEN 1  -- Acute and chronic postprocedural respiratory failure
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J96.1%'    OR CS.FINAL_DX_LIST LIKE '%J96.1%'     THEN 1  -- Chronic respiratory failure, unsp w hypoxia or hypercapnia
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J96.2%'    OR CS.FINAL_DX_LIST LIKE '%J96.2%'     THEN 1  -- Acute and chr resp failure, unsp w hypoxia or hypercapnia
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z99.1%'    OR CS.FINAL_DX_LIST LIKE '%Z99.1%'     THEN 1  -- Dependence on respirator [ventilator] status
             ELSE 0 -- Evaluates for the presence of CHRONIC RESPIRATORY FAILURE
  END AS HX_CHRONIC_RESPIRATORY_FAILURE

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I09.81%'   OR CS.FINAL_DX_LIST LIKE '%I09.81%'    THEN 1  -- Rheumatic heart failure
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13.0%'    OR CS.FINAL_DX_LIST LIKE '%I13.0%'     THEN 1  -- Hypertensive heart and chronic kidney disease with heart failure and stage 1 through stage 4 chronic kidney disease, or unsp
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13.2%'    OR CS.FINAL_DX_LIST LIKE '%I13.2%'     THEN 1  -- Hypertensive heart and chronic kidney disease with heart failure and with stage 5 chronic kidney disease, or end stage renal
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I50%'      OR CS.FINAL_DX_LIST LIKE '%I50%'       THEN 1  -- Left ventricular failure, unspecified
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z95.81%'   OR CS.FINAL_DX_LIST LIKE '%Z95.81%'    THEN 1  -- Presence of heart assist device
             ELSE 0 -- Evaluates for the presence of CONGESTIVE HEART FAILURE
  END AS HX_CONGESTIVE_HEART_FAILURE

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J41%'      OR CS.FINAL_DX_LIST LIKE '%J41%'       THEN 1  -- Simple chronic bronchitis
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J42%'      OR CS.FINAL_DX_LIST LIKE '%J42%'       THEN 1  -- Unspecified chronic bronchitis
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J43%'      OR CS.FINAL_DX_LIST LIKE '%J43%'       THEN 1  -- pulmonary emphysema 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J44%'      OR CS.FINAL_DX_LIST LIKE '%J44%'       THEN 1  -- Chronic obstructive pulmon disease w acute lower resp infct
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J47%'      OR CS.FINAL_DX_LIST LIKE '%J47%'       THEN 1  -- Bronchiectasis with acute lower respiratory infection
             ELSE 0 -- Evaluates for the presence of COPD
  END AS HX_COPD

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F01.5%'    OR CS.FINAL_DX_LIST LIKE '%F01.5%'     THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F02.8%'    OR CS.FINAL_DX_LIST LIKE '%F02.8%'     THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F03.9%'    OR CS.FINAL_DX_LIST LIKE '%F03.9%'     THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%G30%'      OR CS.FINAL_DX_LIST LIKE '%G30%'       THEN 1  -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%G31%'      OR CS.FINAL_DX_LIST LIKE '%G31%'       THEN 1  -- 
             ELSE 0 -- Evaluates for the presence of DEMENTIA
  END AS HX_DEMENTIA

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E10.%'     OR CS.FINAL_DX_LIST LIKE '%E10.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E89.1%'    OR CS.FINAL_DX_LIST LIKE '%E89.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.0%'     OR CS.FINAL_DX_LIST LIKE '%O24.0%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIABETES
  END AS HX_DIABETES_I

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E11.%'     OR CS.FINAL_DX_LIST LIKE '%E11.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.1%'     OR CS.FINAL_DX_LIST LIKE '%O24.1%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIABETES
  END AS HX_DIABETES_II

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E08.22%'   OR CS.FINAL_DX_LIST LIKE '%E08.22%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E08.9%'    OR CS.FINAL_DX_LIST LIKE '%E08.9%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E09.%'     OR CS.FINAL_DX_LIST LIKE '%E09.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E13.%'     OR CS.FINAL_DX_LIST LIKE '%E13.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E89.1%'    OR CS.FINAL_DX_LIST LIKE '%E89.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%N25.1%'    OR CS.FINAL_DX_LIST LIKE '%N25.1%'     THEN 1    -- DO WE REALLY WANT DIABETES INSIPIDOUS?
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.3%'    OR CS.FINAL_DX_LIST LIKE '%O24.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.4%'    OR CS.FINAL_DX_LIST LIKE '%O24.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.8%'    OR CS.FINAL_DX_LIST LIKE '%O24.8%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.9%'    OR CS.FINAL_DX_LIST LIKE '%O24.9%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIABETES
  END AS HX_DIABETES_MIXED

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I95.3%'    OR CS.FINAL_DX_LIST LIKE '%I95.3%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%R88.0%'    OR CS.FINAL_DX_LIST LIKE '%R88.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.502%'  OR CS.FINAL_DX_LIST LIKE '%T81.502%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.512%'  OR CS.FINAL_DX_LIST LIKE '%T81.512%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.522%'  OR CS.FINAL_DX_LIST LIKE '%T81.522%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.532%'  OR CS.FINAL_DX_LIST LIKE '%T81.532%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T81.592%'  OR CS.FINAL_DX_LIST LIKE '%T81.592%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T82.4%'    OR CS.FINAL_DX_LIST LIKE '%T82.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.611%'  OR CS.FINAL_DX_LIST LIKE '%T85.611%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.621%'  OR CS.FINAL_DX_LIST LIKE '%T85.621%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.631%'  OR CS.FINAL_DX_LIST LIKE '%T85.631%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.691'   OR CS.FINAL_DX_LIST LIKE '%T85.691%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%T85.71%'   OR CS.FINAL_DX_LIST LIKE '%T85.71%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Y62.2%'    OR CS.FINAL_DX_LIST LIKE '%Y62.2%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Y84.1%'    OR CS.FINAL_DX_LIST LIKE '%Y84.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z49%'      OR CS.FINAL_DX_LIST LIKE '%Z49%'       THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z91.15%'   OR CS.FINAL_DX_LIST LIKE '%Z91.15%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z99.2%'    OR CS.FINAL_DX_LIST LIKE '%Z99.2%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIALYSIS
  END AS HX_DIALYSIS

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%H35.031%'  OR CS.FINAL_DX_LIST LIKE '%H35.031%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%H35.032%'  OR CS.FINAL_DX_LIST LIKE '%H35.032%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%H35.033%'  OR CS.FINAL_DX_LIST LIKE '%H35.033%'   THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I10.%'     OR CS.FINAL_DX_LIST LIKE '%I10.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I11.%'     OR CS.FINAL_DX_LIST LIKE '%I11.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I12.%'     OR CS.FINAL_DX_LIST LIKE '%I12.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I13.%'     OR CS.FINAL_DX_LIST LIKE '%I13.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I15.%'     OR CS.FINAL_DX_LIST LIKE '%I15.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I16.%'     OR CS.FINAL_DX_LIST LIKE '%I16.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I67.4%'    OR CS.FINAL_DX_LIST LIKE '%I67.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%I97.3%'    OR CS.FINAL_DX_LIST LIKE '%I97.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O10.%'     OR CS.FINAL_DX_LIST LIKE '%O10.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O11.%'     OR CS.FINAL_DX_LIST LIKE '%O11.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O16.%'     OR CS.FINAL_DX_LIST LIKE '%O16.%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of HYPERTENSION
  END AS HX_HYPERTENSION

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D80.%'     OR CS.FINAL_DX_LIST LIKE '%D80.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D81.%'     OR CS.FINAL_DX_LIST LIKE '%D81.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D82.%'     OR CS.FINAL_DX_LIST LIKE '%D82.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D83.%'     OR CS.FINAL_DX_LIST LIKE '%D83.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D84.%'     OR CS.FINAL_DX_LIST LIKE '%D84.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D86.%'     OR CS.FINAL_DX_LIST LIKE '%D86.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%D89.%'     OR CS.FINAL_DX_LIST LIKE '%D89.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%K50.%'     OR CS.FINAL_DX_LIST LIKE '%K50.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%K51.%'     OR CS.FINAL_DX_LIST LIKE '%K51.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%L93.%'     OR CS.FINAL_DX_LIST LIKE '%L93.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M05.%'     OR CS.FINAL_DX_LIST LIKE '%M05.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M06.%'     OR CS.FINAL_DX_LIST LIKE '%M06.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M30.%'     OR CS.FINAL_DX_LIST LIKE '%M30.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M31.%'     OR CS.FINAL_DX_LIST LIKE '%M31.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M32.%'     OR CS.FINAL_DX_LIST LIKE '%M32.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M33.%'     OR CS.FINAL_DX_LIST LIKE '%M33.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M34.%'     OR CS.FINAL_DX_LIST LIKE '%M34.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M35.%'     OR CS.FINAL_DX_LIST LIKE '%M35.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M36.%'     OR CS.FINAL_DX_LIST LIKE '%M36.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M45.%'     OR CS.FINAL_DX_LIST LIKE '%M45.%'      THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M48.8%'    OR CS.FINAL_DX_LIST LIKE '%M48.8%'     THEN 1    --
       WHEN MHX.MEDICAL_HX_LIST LIKE '%M49.%'     OR CS.FINAL_DX_LIST LIKE '%M49.%'      THEN 1    --
             ELSE 0 -- Evaluates for the presence of IMMUNOCOMPROMISED
  END AS HX_IMMUNOCOMPROMISED

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C81.%'     OR CS.FINAL_DX_LIST LIKE '%C81.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C82.%'     OR CS.FINAL_DX_LIST LIKE '%C82.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C83.%'     OR CS.FINAL_DX_LIST LIKE '%C83.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.0%'    OR CS.FINAL_DX_LIST LIKE '%C84.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.1%'    OR CS.FINAL_DX_LIST LIKE '%C84.1%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.4%'    OR CS.FINAL_DX_LIST LIKE '%C84.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.6%'    OR CS.FINAL_DX_LIST LIKE '%C84.6%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.7%'    OR CS.FINAL_DX_LIST LIKE '%C84.7%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.A%'    OR CS.FINAL_DX_LIST LIKE '%C84.A%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C84.Z%'    OR CS.FINAL_DX_LIST LIKE '%C84.Z%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C86.%'     OR CS.FINAL_DX_LIST LIKE '%C86.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C88.%'     OR CS.FINAL_DX_LIST LIKE '%C88.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C90.%'     OR CS.FINAL_DX_LIST LIKE '%C90.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C91.%'     OR CS.FINAL_DX_LIST LIKE '%C91.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C92.%'     OR CS.FINAL_DX_LIST LIKE '%C92.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C93.%'     OR CS.FINAL_DX_LIST LIKE '%C93.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C94.%'     OR CS.FINAL_DX_LIST LIKE '%C94.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C95.%'     OR CS.FINAL_DX_LIST LIKE '%C95.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C96.%'     OR CS.FINAL_DX_LIST LIKE '%C96.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z85.6%'    OR CS.FINAL_DX_LIST LIKE '%Z85.6%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z85.71%'   OR CS.FINAL_DX_LIST LIKE '%Z85.71%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of LYMPHOMA LEUKEMIA MULTI MYELOMA
  END AS HX_LYMPHOMA_LEUKEMIA_MULTI_MYELOMA

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C77.%'     OR CS.FINAL_DX_LIST LIKE '%C77.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C78.%'     OR CS.FINAL_DX_LIST LIKE '%C78.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C79.%'     OR CS.FINAL_DX_LIST LIKE '%C79.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%C7B.%'     OR CS.FINAL_DX_LIST LIKE '%C7B.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%R18.0%'    OR CS.FINAL_DX_LIST LIKE '%R18.0%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of METASTATIC CANCER
  END AS HX_METASTATIC_CANCER

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%E66.%'     OR CS.FINAL_DX_LIST LIKE '%E66.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O99.2%'    OR CS.FINAL_DX_LIST LIKE '%O99.2%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z68.3%'    OR CS.FINAL_DX_LIST LIKE '%Z68.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z68.4%'    OR CS.FINAL_DX_LIST LIKE '%Z68.4%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z68.54%'   OR CS.FINAL_DX_LIST LIKE '%Z68.54%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of OBESITY
  END AS HX_OBESITY

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O13.%'     OR CS.FINAL_DX_LIST LIKE '%013.%'      THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O24.4%'    OR CS.FINAL_DX_LIST LIKE '%O24.4%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of PREGNANCY_COMORBIDITY
  END AS HX_PREGNANCY

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O03.37%'   OR CS.FINAL_DX_LIST LIKE '%O03.37%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O03.87%'   OR CS.FINAL_DX_LIST LIKE '%O03.87%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O04.87%'   OR CS.FINAL_DX_LIST LIKE '%O04.87%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O07.37%'   OR CS.FINAL_DX_LIST LIKE '%O07.37%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O08.82%'   OR CS.FINAL_DX_LIST LIKE '%O08.82%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O75.3%'    OR CS.FINAL_DX_LIST LIKE '%O75.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O85%'      OR CS.FINAL_DX_LIST LIKE '%O85%'       THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O86.04%'   OR CS.FINAL_DX_LIST LIKE '%O86.04%'    THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O98.8%'    OR CS.FINAL_DX_LIST LIKE '%O98.8%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O98.9%'    OR CS.FINAL_DX_LIST LIKE '%O98.9%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of PREGNANCY_DURING_HOSPITALIZATION
  END AS HX_PREGNANCY_DURING_HOSPITALIZATION_SEPSIS

  , CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O98.5%'    OR CS.FINAL_DX_LIST LIKE '%O98.5%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of PREGNANCY_DURING_HOSPITALIZATION
  END AS HX_PREGNANCY_DURING_HOSPITALIZATION_COVID

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%F17.2%'    OR CS.FINAL_DX_LIST LIKE '%F17.2%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%O99.3%'    OR CS.FINAL_DX_LIST LIKE '%O99.3%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%U07%'      OR CS.FINAL_DX_LIST LIKE '%U07%'       THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of SMOKING_VAPING
  END AS HX_SMOKING_VAPING

, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%J95%'      OR CS.FINAL_DX_LIST LIKE '%J95%'       THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z43.0%'    OR CS.FINAL_DX_LIST LIKE '%Z43.0%'     THEN 1    -- 
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z93.0%'    OR CS.FINAL_DX_LIST LIKE '%Z93.0%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of TRACHEOSTOMY_ARRIVAL
  END AS HX_TRACHEOSTOMY_ARRIVAL
  
, CASE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.0%'    OR CS.FINAL_DX_LIST LIKE '%Z74.0%'     THEN 5    -- KIDNEY
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.1%'    OR CS.FINAL_DX_LIST LIKE '%Z74.1%'     THEN 3    -- HEART
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.2%'    OR CS.FINAL_DX_LIST LIKE '%Z74.2%'     THEN 4    -- LUNG
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.3%'    OR CS.FINAL_DX_LIST LIKE '%Z74.3%'     THEN 6    -- HEAR AND LUNG
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.4%'    OR CS.FINAL_DX_LIST LIKE '%Z74.4%'     THEN 2    -- LIVER
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.5%'    OR CS.FINAL_DX_LIST LIKE '%Z74.5%'     THEN 9    -- OTHER - SKIN
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.6%'    OR CS.FINAL_DX_LIST LIKE '%Z74.6%'     THEN 9    -- OTHER - BONE
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.7%'    OR CS.FINAL_DX_LIST LIKE '%Z74.7%'     THEN 9    -- OTHER
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.81%'   OR CS.FINAL_DX_LIST LIKE '%Z74.81%'    THEN 1    -- BMT
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.84%'   OR CS.FINAL_DX_LIST LIKE '%Z74.84%'    THEN 1    -- STEM CELL
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.8%'    OR CS.FINAL_DX_LIST LIKE '%Z74.8%'     THEN 9    -- OTHER
       WHEN MHX.MEDICAL_HX_LIST LIKE '%Z74.9%'    OR CS.FINAL_DX_LIST LIKE '%Z74.9%'     THEN 9    -- OTHER
             ELSE 0 -- Evaluates for the presence of TRACHEOSTOMY_ARRIVAL
  END AS HX_TRANSPLANT

/* ------------------------------------------------------------------------
! SEVERITY OF ILLNESS - CURRENT ADMISSION
!------------------------------------------------------------------------- */

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%G45%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I21%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I22%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I60%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I61%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I63%'        THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACUTE CARDIOVASCULAR
  END AS CC_ACUTE_CARDIOVASCULAR

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%G93.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.0%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.20%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.242%'    THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R40.3%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R41.82%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ALTERED MENTAL STATUS
  END AS CC_ALTERED_MENTAL_STATUS

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%D65%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D66%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D68.%'       THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D69%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%D75.82%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%M36.2%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%O72.3%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R79.1%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COAGULOPATHY
  END AS CC_COAGULOPATHY

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%I05%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I06%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I07%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I08%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I09%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I20%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I23%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I24%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I25%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I34%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I35%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I36%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I37%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I65%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I66%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I67%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I68%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I69%'        THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.02%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.03%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.04%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.05%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.06%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.07%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.08%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I70.09%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I73.89%'     THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%I73.9%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.2%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.3%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.4%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.5%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z95.820%'    THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z98.62%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of OTHER CARDIOVASCULAR
  END AS CC_OTHER_CARDIOVASCULAR

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.2%'      THEN 1    --  
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z20.826%' OR CS.FINAL_DX_LIST LIKE '%Z20.828%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS CC_COVID_POSITIVE

, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%J10%'        THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of INFLUENZA (KNOWN OR SUSPECTED)
  END AS CC_INFLUENZA_POSITIVE
  
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%J96.01%'     THEN 1    -- Acute respiratory failure with hypoxia
       WHEN  CS.FINAL_DX_LIST LIKE '%J96.21%'     THEN 1    -- acute on chronic respiratory failure with hypoxia
       WHEN  CS.FINAL_DX_LIST LIKE '%J96.91%'     THEN 1    -- respiratory failure with hypoxia, unspecified
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_ACUTE_HYPOXIC_RESP_FAILURE
  
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%J96%'     THEN 1    -- Acute respiratory failure with hypoxia
       WHEN  CS.FINAL_DX_LIST LIKE '%R09.2%'   THEN 1    -- Acute respiratory ARREST
       WHEN  CS.FINAL_DX_LIST LIKE '%P28.5%'   THEN 1    -- respiratory failure of newborn
       WHEN  CS.FINAL_DX_LIST LIKE '%P28.81%'  THEN 1    -- RESPIRATORY ARREST PEDS
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_ACUTE_RESP_FAILURE
    
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%J80%'     THEN 1    -- ARDS
       WHEN  CS.FINAL_DX_LIST LIKE '%P22.0%'   THEN 1    -- PEDIATRIC ARDS
             ELSE 0 -- Evaluates for the presence of ARDS
  END AS CC_ARDS
   
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%M35.8%'   AND CS.FINAL_DX_LIST LIKE '%U07.1%'   THEN 1    -- PEDIATRIC MISC   https://www.cdc.gov/mis-c/hcp/
       WHEN  CS.FINAL_DX_LIST LIKE '%B94.8%'   AND CS.FINAL_DX_LIST LIKE '%M35.8%'   THEN 1    -- PEDIATRIC MISC  https://downloads.aap.org/AAP/PDF/COVID%202020.pdf
             ELSE 0 -- Evaluates for the presence of MISC
  END AS CC_PEDS_MISC
     
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%K76.3%'   THEN 1    -- LIVER INFARCT
       WHEN  CS.FINAL_DX_LIST LIKE '%K22.0%'   THEN 1    -- Acute and subacute hepatic failure without coma
       WHEN  CS.FINAL_DX_LIST LIKE '%K22.9%'   THEN 1    -- Hepatic failure, unspecified
       WHEN  CS.FINAL_DX_LIST LIKE '%K91.82%'  THEN 1    -- Postprocedural hepatic failure
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_ACUTE_LIVER_INJURY
       
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%R78.81%'  THEN 1    -- LIVER INFARCT
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_BACTEREMIA
  
, CASE
       WHEN  (CS.FINAL_DX_LIST LIKE '%N17%'     OR
              CS.FINAL_DX_LIST LIKE '%N19%'     OR
              CS.FINAL_DX_LIST LIKE '%N28.9%'   )
             AND 
             (PCS.PX_LIST NOT LIKE '%SA1D00Z%' OR
              PCS.PX_LIST NOT LIKE '%SA1D60Z%' OR
              PCS.PX_LIST NOT LIKE '%SA1D70Z%' OR
              PCS.PX_LIST NOT LIKE '%SA1D80Z%' OR
              PCS.PX_LIST NOT LIKE '%SA1D90Z%' OR
              PCS.PX_LIST NOT LIKE '%3E1M39Z%' )
             THEN 1    -- LIVER INFARCT
             ELSE 0 -- Evaluates for the presence of AKI/ARF WITHOUT DIALYSIS
  END AS CC_ACUTE_RENAL_INJURY_N_HEMODIALYSIS
  
, CASE
       WHEN  (CS.FINAL_DX_LIST LIKE '%N17%'     OR
              CS.FINAL_DX_LIST LIKE '%N19%'     OR
              CS.FINAL_DX_LIST LIKE '%N28.9%'   )
             AND 
             (PCS.PX_LIST LIKE '%SA1D00Z%'      OR
              PCS.PX_LIST LIKE '%SA1D60Z%'      OR
              PCS.PX_LIST LIKE '%SA1D70Z%'      OR
              PCS.PX_LIST LIKE '%SA1D80Z%'      OR
              PCS.PX_LIST LIKE '%SA1D90Z%'      OR
              PCS.PX_LIST LIKE '%3E1M39Z%' )
             THEN 1    -- LIVER INFARCT
             ELSE 0 -- Evaluates for the presence of AKI/ARF WITHOUT DIALYSIS
  END AS CC_ACUTE_RENAL_FAILURE_R_HEMODIALYSIS
  
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%J15%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of BACTERIAL PNEUMONIA
  END AS CC_BACTERIAL_PNEUMONIA
  
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%I46%'     THEN 1    -- LIVER INFARCT
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_CARDIAC_ARREST
 
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%P28.81%'  THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%R09.2%'   THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_RESPIRATORY_ARREST
  
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%I48%'     AND MHX.MEDICAL_HX_LIST NOT LIKE '%I48%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_ATRIAL_FIB_NEW
  
, CASE
       WHEN  CS.FINAL_DX_LIST LIKE '%I45%'     AND MHX.MEDICAL_HX_LIST NOT LIKE '%I45%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACUTE HYPOXIC RESP FAILURE
  END AS CC_HEART_BLOCK_NEW


/* ------------------------------------------------------------------------
! TREATMENT VARIABLES
!------------------------------------------------------------------------- */

, CASE 
       WHEN  PCS.PX_LIST LIKE '%SA1D00Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D60Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D70Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D80Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%SA1D90Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E1M39Z%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of DIALYSIS TREATMENT
  END AS T_DIALYSIS

, CASE 
       WHEN  PCS.PX_LIST LIKE '%SA1D00Z%' OR PCS.PX_LIST LIKE '%SA1D60Z%' OR PCS.PX_LIST LIKE '%SA1D70Z%' OR
             PCS.PX_LIST LIKE '%SA1D80Z%' OR PCS.PX_LIST LIKE '%SA1D90Z%' OR PCS.PX_LIST LIKE '%3E1M39Z%' 
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, 'SA1D00Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'SA1D00Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'SA1D60Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'SA1D60Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,    
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'SA1D70Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'SA1D70Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'SA1D80Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'SA1D80Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END, 
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'SA1D90Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'SA1D90Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E1M39Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E1M39Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of NON-INVASIVE VENTILATION TREATMENT FIRST START
  END AS T_DIALYSIS_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%5A15223%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1522F%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1522G%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1522H%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A15A2F%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A15A2G%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A15A2H%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ECMO TREATMENT
  END AS T_ECMO

, CASE 
       WHEN  PCS.PX_LIST LIKE '%5A15223%' OR PCS.PX_LIST LIKE '%5A1522F%' OR PCS.PX_LIST LIKE '%5A1522G%' OR
             PCS.PX_LIST LIKE '%5A1522H%' OR PCS.PX_LIST LIKE '%5A15A2F%' OR PCS.PX_LIST LIKE '%5A15A2G%' OR
             PCS.PX_LIST LIKE '%5A15A2H%'
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '5A15223 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A15223 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A1522F - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A1522F - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,    
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A1522G - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A1522G - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A1522H - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A1522H - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END, 
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A15A2F - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A15A2F - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A15A2G - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A15A2G - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A15A2H - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A15A2H - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of ECMO FIRST START
  END AS T_ECMO_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%09HN7BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09HN8BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09H13EZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09H17EZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%09H18EZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CHY7BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CHY8BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CH57BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0CH58BZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0WHQ73Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0WHQ7YZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1935Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1945Z%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A1955Z%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of MECHANICAL VENTILATION TREATMENT
  END AS T_MECHANICAL_VENTILATION

, CASE 
       WHEN  PCS.PX_LIST LIKE '%5A1935Z%' OR PCS.PX_LIST LIKE '%5A1945Z%' OR PCS.PX_LIST LIKE '%5A1955Z%' OR
             PCS.PX_LIST LIKE '%09HN7BZ%' OR PCS.PX_LIST LIKE '%09HN8BZ%' OR PCS.PX_LIST LIKE '%09H13EZ%' OR
             PCS.PX_LIST LIKE '%09H17EZ%' OR PCS.PX_LIST LIKE '%09H18EZ%' OR PCS.PX_LIST LIKE '%0CHY7BZ%' OR
             PCS.PX_LIST LIKE '%0CHY8BZ%' OR PCS.PX_LIST LIKE '%0CH57BZ%' OR PCS.PX_LIST LIKE '%0CH58BZ%' OR
             PCS.PX_LIST LIKE '%0WHQ73Z%' OR PCS.PX_LIST LIKE '%0WHQ7YZ%'     THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '5A1935Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A1935Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A1945Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A1945Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,    
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A1955Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A1955Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '09HN7BZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '09HN7BZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END, 
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '09HN8BZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '09HN8BZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '09H13EZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '09H13EZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '09H17EZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '09H17EZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '09H18EZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '09H18EZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0CHY7BZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0CHY7BZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0CHY8BZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0CHY8BZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0CH57BZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0CH57BZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0CH58BZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0CH58BZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0WHQ73Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0WHQ73Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0WHQ7YZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0WHQ7YZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of MECHANICAL VENTILATION TREATMENT FIRST START
  END AS T_MECHANICAL_VENTILATION_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%5A09357%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09358%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09457%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09458%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09557%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%5A09558%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of NON-INVASIVE VENTILATION TREATMENT
  END AS T_NI_VENTILATION

, CASE 
       WHEN  PCS.PX_LIST LIKE '%5A09357%' OR PCS.PX_LIST LIKE '%5A09358%' OR PCS.PX_LIST LIKE '%5A09457%' OR
             PCS.PX_LIST LIKE '%5A09458%' OR PCS.PX_LIST LIKE '%5A09557%' OR PCS.PX_LIST LIKE '%5A09558%' 
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '5A09357 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A09357 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A09358 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A09358 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,    
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A09457 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A09457 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A09458 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A09458 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END, 
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A09557 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A09557 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '5A09558 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '5A09558 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of NON-INVASIVE VENTILATION TREATMENT FIRST START
  END AS T_NI_VENTILATION_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033E5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW043E5%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of REMDESIVIR TREATMENT
  END AS T_REMDESIVIR

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033E5%' OR PCS.PX_LIST LIKE '%XW043E5%' 
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, 'XW033E5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW033E5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'XW043E5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW043E5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of REMDESIVIR TREATMENT FIRST START
  END AS T_REMDESIVIR_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033G5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW043G5%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of Sarilumab TREATMENT
  END AS T_SARILUMAB

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033G5%' OR PCS.PX_LIST LIKE '%XW043G5%' 
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, 'XW033G5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW033G5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'XW043G5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW043G5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of Sarilumab TREATMENT FIRST START
  END AS T_SARILUMAB_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033H5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW043H5%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of Tocilizumab TREATMENT
  END AS T_TOCILIZUMAB

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW033H5%' OR PCS.PX_LIST LIKE '%XW043H5%' 
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, 'XW033H5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW033H5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'XW043H5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW043H5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of TOCILIZUMAB TREATMENT FIRST START
  END AS T_TOCILIZUMAB_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW13325%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW14325%'         THEN 1    --  
             ELSE 0 -- Evaluates for the presence of CONVALESCENT PLASMA TREATMENT
  END AS T_CON_PLASMA

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW13325%' OR PCS.PX_LIST LIKE '%XW14325%' 
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, 'XW13325 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW13325 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'XW14325 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW14325 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of CONVALESCENT PLASMA TREATMENT FIRST START
  END AS T_CON_PLASMA_DATE

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW013F5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW033F5%'         THEN 1    --   
       WHEN  PCS.PX_LIST LIKE '%XW043F5%'         THEN 1    --  
       WHEN  PCS.PX_LIST LIKE '%XW0DXF5%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of NOVEL TREATMENTS
  END AS T_NOVEL_TREATMENTS

, CASE 
       WHEN  PCS.PX_LIST LIKE '%XW013F5%' OR PCS.PX_LIST LIKE '%XW033F5%' OR PCS.PX_LIST LIKE '%XW043F5%' OR
             PCS.PX_LIST LIKE '%XW0DXF5%' THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, 'XW013F5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW013F5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'XW033F5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW033F5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'XW043F5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW043F5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, 'XW0DXF5 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, 'XW0DXF5 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of NOVEL TREATMENTS FIRST START
  END AS T_NOVEL_TREATMENTS_DATE

, CASE
       WHEN  PCS.PX_LIST LIKE '%0230J1%'          THEN 1    -- ALBUMIN - PERIPHERAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0233J1%'          THEN 1    -- ALBUMIN - PERIPHERAL PERC
       WHEN  PCS.PX_LIST LIKE '%0240J1%'          THEN 1    -- ALBUMIN - CENTRAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0243J1%'          THEN 1    -- ALBUMIN - CENTRAL PERC
             ELSE 0 -- Evaluates for the presence of ALBUMIN
  END AS T_ALBUMIN

, CASE 
       WHEN  PCS.PX_LIST LIKE '%0230J1%' OR PCS.PX_LIST LIKE '%0233J1%' OR PCS.PX_LIST LIKE '%0240J1%' OR
             PCS.PX_LIST LIKE '%0243J1%' THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '0230J1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0230J1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0233J1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0233J1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0240J1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0240J1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0243J1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0243J1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of ALBUMIN FIRST START
  END AS T_ALBUMIN_DATE
  
, CASE
       WHEN  PCS.PX_LIST LIKE '%0230M1%'          THEN 1    -- CRYO - PERIPHERAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0233M1%'          THEN 1    -- CRYO - PERIPHERAL PERC
       WHEN  PCS.PX_LIST LIKE '%0240M1%'          THEN 1    -- CRYO - CENTRAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0243M1%'          THEN 1    -- CRYO - CENTRAL PERC
             ELSE 0 -- Evaluates for the presence of CRYO
  END AS T_CRYO

, CASE 
       WHEN  PCS.PX_LIST LIKE '%0230M1%' OR PCS.PX_LIST LIKE '%0233M1%' OR PCS.PX_LIST LIKE '%0240M1%' OR
             PCS.PX_LIST LIKE '%0243M1%' THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '0230M1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0230M1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0233M1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0233M1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0240M1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0240M1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0243M1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0243M1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of CRYO FIRST START
  END AS T_CRYO_DATE
    
, CASE
       WHEN  PCS.PX_LIST LIKE '%0230K1%'          THEN 1    -- FFP - PERIPHERAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0233K1%'          THEN 1    -- FFP - PERIPHERAL PERC
       WHEN  PCS.PX_LIST LIKE '%0240K1%'          THEN 1    -- FFP - CENTRAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0243K1%'          THEN 1    -- FFP - CENTRAL PERC
             ELSE 0 -- Evaluates for the presence of FFP
  END AS T_FFP

, CASE 
       WHEN  PCS.PX_LIST LIKE '%0230K1%' OR PCS.PX_LIST LIKE '%0233K1%' OR PCS.PX_LIST LIKE '%0240K1%' OR
             PCS.PX_LIST LIKE '%0243K1%' THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '0230K1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0230K1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0233K1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0233K1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0240K1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0240K1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0243K1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0243K1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of FFP FIRST START
  END AS T_FFP_DATE
  
, CASE
       WHEN  PCS.PX_LIST LIKE '%0230R1%'          THEN 1    -- PLATELETS - PERIPHERAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0233R1%'          THEN 1    -- PLATELETS - PERIPHERAL PERC
       WHEN  PCS.PX_LIST LIKE '%0240R1%'          THEN 1    -- PLATELETS - CENTRAL OPEN
       WHEN  PCS.PX_LIST LIKE '%0243R1%'          THEN 1    -- PLATELETS - CENTRAL PERC
             ELSE 0 -- Evaluates for the presence of NOVEL TREATMENTS
  END AS T_PLATELETS

, CASE 
       WHEN  PCS.PX_LIST LIKE '%0230R1%' OR PCS.PX_LIST LIKE '%0233R1%' OR PCS.PX_LIST LIKE '%0240R1%' OR
             PCS.PX_LIST LIKE '%0243K1%' THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '0230R1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0230R1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0233R1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0233R1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0240R1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0240R1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '0243R1 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '0243R1 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of PLATELETS FIRST START
  END AS T_PLATELETS_DATE
    
, CASE
       WHEN  PCS.PX_LIST LIKE '%3E03016%'         THEN 1    -- THROMBOLYTIC - PERIPHERAL OPEN
       WHEN  PCS.PX_LIST LIKE '%3E03017%'         THEN 1    -- THROMBOLYTIC - PERIPHERAL PERC
       WHEN  PCS.PX_LIST LIKE '%3E030PZ%'         THEN 1    -- THROMBOLYTIC - CENTRAL OPEN
       WHEN  PCS.PX_LIST LIKE '%3E03316%'         THEN 1    -- THROMBOLYTIC - CENTRAL PERC
       WHEN  PCS.PX_LIST LIKE '%3E03317%'         THEN 1    -- THROMBOLYTIC - CENTRAL PERC
       WHEN  PCS.PX_LIST LIKE '%3E033PZ%'         THEN 1    -- THROMBOLYTIC - CENTRAL PERC
       WHEN  PCS.PX_LIST LIKE '%3E04016%'         THEN 1    -- THROMBOLYTIC - PERIPHERAL OPEN
       WHEN  PCS.PX_LIST LIKE '%3E04017%'         THEN 1    -- THROMBOLYTIC - PERIPHERAL PERC
       WHEN  PCS.PX_LIST LIKE '%3E040PZ%'         THEN 1    -- THROMBOLYTIC - CENTRAL OPEN
       WHEN  PCS.PX_LIST LIKE '%3E04316%'         THEN 1    -- THROMBOLYTIC - CENTRAL PERC
       WHEN  PCS.PX_LIST LIKE '%3E04317%'         THEN 1    -- THROMBOLYTIC - CENTRAL PERC
       WHEN  PCS.PX_LIST LIKE '%3E043PZ%'         THEN 1    -- THROMBOLYTIC - CENTRAL PERC
             ELSE 0 -- Evaluates for the presence of THROMBOTICS
  END AS T_THROMBOLYTIC

, CASE 
       WHEN  PCS.PX_LIST LIKE '%3E03016%' OR PCS.PX_LIST LIKE '%3E03017%' OR PCS.PX_LIST LIKE '%3E030PZ%' OR
             PCS.PX_LIST LIKE '%3E03316%' OR PCS.PX_LIST LIKE '%3E03317%' OR PCS.PX_LIST LIKE '%3E033PZ%' OR
             PCS.PX_LIST LIKE '%3E04016%' OR PCS.PX_LIST LIKE '%3E04017%' OR PCS.PX_LIST LIKE '%3E040PZ%' OR
             PCS.PX_LIST LIKE '%3E04316%' OR PCS.PX_LIST LIKE '%3E04317%' OR PCS.PX_LIST LIKE '%3E043PZ%' 
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '3E03016 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E03016 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E03017 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E03017 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E030PZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E030PZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E03316 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E03316 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E03317 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E03317 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E033PZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E033PZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E04016 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E04016 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E04017 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E04017 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E040PZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E040PZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E04316 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E04316 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E04317 - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E04317 - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E043PZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E043PZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of THROMBOLYTIC FIRST START 
  END AS T_THROMBOLYTIC_DATE
    
, CASE
       WHEN  PCS.PX_LIST LIKE '%3E030XZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E033XZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E040XZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E043XZ%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of VASOPRESSORS
  END AS T_VASOPRESSOR

, CASE 
       WHEN  PCS.PX_LIST LIKE '%3E030XZ%' OR PCS.PX_LIST LIKE '%3E033XZ%' OR PCS.PX_LIST LIKE '%3E040XZ%' OR
             PCS.PX_LIST LIKE '%3E043XZ%' THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '3E030XZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E030XZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E033XZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E033XZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E040XZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E040XZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E043XZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E043XZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of VASOPRESSOR FIRST START
  END AS T_VASOPRESSOR_DATE
    
, CASE
       WHEN  PCS.PX_LIST LIKE '%3E030NZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E033NZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E040NZ%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E043NZ%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of SEDATIVES
  END AS T_SEDATIVES

, CASE 
       WHEN  PCS.PX_LIST LIKE '%3E030NZ%' OR PCS.PX_LIST LIKE '%3E033NZ%' OR PCS.PX_LIST LIKE '%3E040NZ%' OR
             PCS.PX_LIST LIKE '%3E043NZ%' THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '3E030NZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E030NZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E033NZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E033NZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E040NZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E040NZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '3E043NZ - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '3E043NZ - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of SEDATIVES FIRST START
  END AS T_SEDATIVES_DATE
    
, CASE
       WHEN  PCS.PX_LIST LIKE '%3E03002%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E03003%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E03005%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E0300M%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E0300P%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E03302%'         THEN 1    --
       WHEN  PCS.PX_LIST LIKE '%3E03303%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E03305%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E0330M%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E0330P%'         THEN 1    -- 

       WHEN  PCS.PX_LIST LIKE '%3E04002%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E04003%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E04005%'         THEN 1    --
       WHEN  PCS.PX_LIST LIKE '%3E0400M%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E0400P%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E04302%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E04303%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E04305%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E0430M%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%3E0430P%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of CHEMO
  END AS T_CHEMO

  , CASE
       WHEN  PCS.PX_LIST LIKE '%05HN03Z%'         THEN 1    -- L IJ OPEN
       WHEN  PCS.PX_LIST LIKE '%05HN33Z%'         THEN 1    -- L IJ PERC
       WHEN  PCS.PX_LIST LIKE '%05HN43Z%'         THEN 1    -- L IJ PERC
       WHEN  PCS.PX_LIST LIKE '%05HM03Z%'         THEN 1    -- R IJ OPEN
       WHEN  PCS.PX_LIST LIKE '%05HM33Z%'         THEN 1    -- R IJ PERC
       WHEN  PCS.PX_LIST LIKE '%05HM43Z%'         THEN 1    -- R IJ ENDO
       WHEN  PCS.PX_LIST LIKE '%05H603Z%'         THEN 1    -- L SC OPEN
       WHEN  PCS.PX_LIST LIKE '%05H633Z%'         THEN 1    -- L SC PERC
       WHEN  PCS.PX_LIST LIKE '%05H643Z%'         THEN 1    -- L SC ENDO
       WHEN  PCS.PX_LIST LIKE '%05H503Z%'         THEN 1    -- R SC OPEN
       WHEN  PCS.PX_LIST LIKE '%05H533Z%'         THEN 1    -- R SC PERC
       WHEN  PCS.PX_LIST LIKE '%05H543Z%'         THEN 1    -- R SC ENDO
       WHEN  PCS.PX_LIST LIKE '%06HN03Z%'         THEN 1    -- L FEM OPEN
       WHEN  PCS.PX_LIST LIKE '%06HN33Z%'         THEN 1    -- L FEM PERC
       WHEN  PCS.PX_LIST LIKE '%06HN43Z%'         THEN 1    -- L FEM ENDO
       WHEN  PCS.PX_LIST LIKE '%06HM03Z%'         THEN 1    -- R FEM OPEN
       WHEN  PCS.PX_LIST LIKE '%06HM33Z%'         THEN 1    -- R FEM PERC
       WHEN  PCS.PX_LIST LIKE '%06HM43Z%'         THEN 1    -- R FEM ENDO
             ELSE 0 -- Evaluates for the presence of CENTRAL ACCESS
  END AS T_CENTRAL_ACCESS

, CASE 
       WHEN  PCS.PX_LIST LIKE '%05HN03Z%' OR PCS.PX_LIST LIKE '%05HN33Z%' OR PCS.PX_LIST LIKE '%05HN43Z%' OR  -- L IJ
             PCS.PX_LIST LIKE '%05HM03Z%' OR PCS.PX_LIST LIKE '%05HM33Z%' OR PCS.PX_LIST LIKE '%05HM43Z%' OR  -- R IJ
             PCS.PX_LIST LIKE '%05H603Z%' OR PCS.PX_LIST LIKE '%05H633Z%' OR PCS.PX_LIST LIKE '%05H643Z%' OR  -- L SC
             PCS.PX_LIST LIKE '%05H503Z%' OR PCS.PX_LIST LIKE '%05H533Z%' OR PCS.PX_LIST LIKE '%05H543Z%' OR  -- R SC
             PCS.PX_LIST LIKE '%06HN03Z%' OR PCS.PX_LIST LIKE '%06HN33Z%' OR PCS.PX_LIST LIKE '%06HN43Z%' OR  -- L FEM
             PCS.PX_LIST LIKE '%06HM03Z%' OR PCS.PX_LIST LIKE '%06HM33Z%' OR PCS.PX_LIST LIKE '%06HM43Z%'     -- R FEM
             THEN 
                         LEAST(   CASE WHEN regexp_substr(PCS.PX_LIST, '05HN03Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
				ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05HN03Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05HN33Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05HN33Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,  
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05HN43Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05HN43Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,   
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05HM03Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD') 
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05HM03Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05HM33Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05HM33Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END, 
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05HM43Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05HM43Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05H603Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05H603Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05H633Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05H633Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05H643Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05H643Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05H503Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05H503Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,  
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05H533Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05H533Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '05H543Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '05H543Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '06HN03Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '06HN03Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '06HN33Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '06HN33Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '06HN43Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '06HN43Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '06HM03Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '06HM03Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '06HM33Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '06HM33Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END,
                                  CASE WHEN regexp_substr(PCS.PX_LIST, '06HM43Z - [^- ]+', 1, 1) IS NULL THEN TO_DATE('9999-12-31','YYYY-MM-DD')  
                                         ELSE TO_DATE(SUBSTR(TRIM(TRAILING ',' FROM (regexp_substr(PCS.PX_LIST, '06HM43Z - [^- ]+', 1, 1))), -10),'YYYY/MM/DD') END
                               ) 
             ELSE NULL -- Evaluates for the presence of CENTRAL ACCESS FIRST START
  END AS T_CENTRAL_ACCESS_DATE

/* ------------------------------------------------------------------------
! CLINICAL VARIABLES
!------------------------------------------------------------------------- */


, CASE 
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.1%'      THEN 1    -- 
       WHEN  CS.FINAL_DX_LIST LIKE '%U07.2%'      THEN 1    --  
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z20.826%' OR CS.FINAL_DX_LIST LIKE '%Z20.828%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS C_COVID_VIRUS
  
, CASE 
       WHEN  CS.FINAL_DX_LIST LIKE '%Z20.828%'    THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS C_COVID_EXPOSURE
    
, CASE 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A41.02%'  OR CS.FINAL_DX_LIST LIKE '%A41.02%'      THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A49.02%'  OR CS.FINAL_DX_LIST LIKE '%A49.02%'      THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J15.212%' OR CS.FINAL_DX_LIST LIKE '%J15.212%'     THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z16%'     OR CS.FINAL_DX_LIST LIKE '%Z16%'         THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z22.322%' OR CS.FINAL_DX_LIST LIKE '%Z22.322%'     THEN 1    -- 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%Z86.14%'  OR CS.FINAL_DX_LIST LIKE '%Z86.14%'      THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of COVID POSITIVE (KNOWN OR SUSPECTED)
  END AS C_DRUG_RESISTANT_PATHOGEN
      
, CASE 
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N10%'     OR CS.FINAL_DX_LIST LIKE '%N10%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N11%'     OR CS.FINAL_DX_LIST LIKE '%N11%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N13.6%'   OR CS.FINAL_DX_LIST LIKE '%N13.6%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N15.1%'   OR CS.FINAL_DX_LIST LIKE '%N15.1%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N15.9%'   OR CS.FINAL_DX_LIST LIKE '%N15.9%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N16%'     OR CS.FINAL_DX_LIST LIKE '%N16%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N28.84%'  OR CS.FINAL_DX_LIST LIKE '%N28.84%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J0%'      OR CS.FINAL_DX_LIST LIKE '%J0%'          THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J1%'      OR CS.FINAL_DX_LIST LIKE '%J1%'          THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J20%'     OR CS.FINAL_DX_LIST LIKE '%J20%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J44.0%'   OR CS.FINAL_DX_LIST LIKE '%J44.0%'       THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J44.1%'   OR CS.FINAL_DX_LIST LIKE '%J44.1%'       THEN 2    -- RESPIRATORY    BY DEFINITION, THIS ONE DOESNT INDICATE INFECTION PRESENT
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J49%'     OR CS.FINAL_DX_LIST LIKE '%J49%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J69%'     OR CS.FINAL_DX_LIST LIKE '%J69%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J85%'     OR CS.FINAL_DX_LIST LIKE '%J85%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%J86%'     OR CS.FINAL_DX_LIST LIKE '%J86%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A02%'     OR CS.FINAL_DX_LIST LIKE '%A02%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A08%'     OR CS.FINAL_DX_LIST LIKE '%A08%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A09%'     OR CS.FINAL_DX_LIST LIKE '%A09%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A15%'     OR CS.FINAL_DX_LIST LIKE '%A15%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A17.83%'  OR CS.FINAL_DX_LIST LIKE '%A17.83%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A17.9%'   OR CS.FINAL_DX_LIST LIKE '%A17.9%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A17%'     OR CS.FINAL_DX_LIST LIKE '%A17%'         THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.0%'   OR CS.FINAL_DX_LIST LIKE '%A18.0%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.1%'   OR CS.FINAL_DX_LIST LIKE '%A18.1%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.2%'   OR CS.FINAL_DX_LIST LIKE '%A18.2%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.0%'   OR CS.FINAL_DX_LIST LIKE '%A18.0%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.83%'  OR CS.FINAL_DX_LIST LIKE '%A18.83%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18%'     OR CS.FINAL_DX_LIST LIKE '%A18%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A31.0%'   OR CS.FINAL_DX_LIST LIKE '%A31.0%'       THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A31.1%'   OR CS.FINAL_DX_LIST LIKE '%A31.1%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A32.0%'   OR CS.FINAL_DX_LIST LIKE '%A32.0%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A32.1%'   OR CS.FINAL_DX_LIST LIKE '%A32.1%'       THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A36.3%'   OR CS.FINAL_DX_LIST LIKE '%A36.3%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A36%'     OR CS.FINAL_DX_LIST LIKE '%A36%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A37%'     OR CS.FINAL_DX_LIST LIKE '%A37%'         THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A39%'     OR CS.FINAL_DX_LIST LIKE '%A39%'         THEN 5    -- CNS - MENINGITIS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A18.0%'   OR CS.FINAL_DX_LIST LIKE '%A18.0%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.0%'   OR CS.FINAL_DX_LIST LIKE '%A42.0%'       THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.1%'   OR CS.FINAL_DX_LIST LIKE '%A42.1%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.7%'   OR CS.FINAL_DX_LIST LIKE '%A42.7%'       THEN 8    -- SEPTICEMIA
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.82%'  OR CS.FINAL_DX_LIST LIKE '%A42.82%'      THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42.81%'  OR CS.FINAL_DX_LIST LIKE '%A42.81%'      THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A43.0%'   OR CS.FINAL_DX_LIST LIKE '%A43.0%'       THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A43.1%'   OR CS.FINAL_DX_LIST LIKE '%A43.1%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A46%'     OR CS.FINAL_DX_LIST LIKE '%A46%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A48.1%'   OR CS.FINAL_DX_LIST LIKE '%A48.1%'       THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.04%'  OR CS.FINAL_DX_LIST LIKE '%A50.04%'      THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.06%'  OR CS.FINAL_DX_LIST LIKE '%A50.06%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.07%'  OR CS.FINAL_DX_LIST LIKE '%A50.07%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50.4%'   OR CS.FINAL_DX_LIST LIKE '%A50.4%'       THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A51.3%'   OR CS.FINAL_DX_LIST LIKE '%A51.3%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A51.41%'  OR CS.FINAL_DX_LIST LIKE '%A51.41%'      THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.1%'   OR CS.FINAL_DX_LIST LIKE '%A52.1%'       THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.2%'   OR CS.FINAL_DX_LIST LIKE '%A52.2%'       THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.3%'   OR CS.FINAL_DX_LIST LIKE '%A52.3%'       THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.72%'  OR CS.FINAL_DX_LIST LIKE '%A52.72%'      THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52.73%'  OR CS.FINAL_DX_LIST LIKE '%A52.73%'      THEN 2    -- RESPIRATORY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.85%'  OR CS.FINAL_DX_LIST LIKE '%A54.85%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.86%'  OR CS.FINAL_DX_LIST LIKE '%A54.86%'      THEN 8    -- SEPTICEMIA
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.89%'  OR CS.FINAL_DX_LIST LIKE '%A54.89%'      THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54.8%'   OR CS.FINAL_DX_LIST LIKE '%A54.8%'       THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A54%'     OR CS.FINAL_DX_LIST LIKE '%A54%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B78.1%'   OR CS.FINAL_DX_LIST LIKE '%B78.1%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%E83.2%'   OR CS.FINAL_DX_LIST LIKE '%E83.2%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%G0%'      OR CS.FINAL_DX_LIST LIKE '%G0%'          THEN 5    -- CNS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%I80%'     OR CS.FINAL_DX_LIST LIKE '%I80%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K12.2%'   OR CS.FINAL_DX_LIST LIKE '%K12.2%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K35%'     OR CS.FINAL_DX_LIST LIKE '%K35%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K36%'     OR CS.FINAL_DX_LIST LIKE '%K36%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K37%'     OR CS.FINAL_DX_LIST LIKE '%K37%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.014%' OR CS.FINAL_DX_LIST LIKE '%K50.014%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.114%' OR CS.FINAL_DX_LIST LIKE '%K50.114%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.814%' OR CS.FINAL_DX_LIST LIKE '%K50.814%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K50.914%' OR CS.FINAL_DX_LIST LIKE '%K50.914%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.014%' OR CS.FINAL_DX_LIST LIKE '%K51.014%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.214%' OR CS.FINAL_DX_LIST LIKE '%K51.214%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.314%' OR CS.FINAL_DX_LIST LIKE '%K51.314%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.414%' OR CS.FINAL_DX_LIST LIKE '%K51.414%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.514%' OR CS.FINAL_DX_LIST LIKE '%K51.514%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.814%' OR CS.FINAL_DX_LIST LIKE '%K51.814%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K51.914%' OR CS.FINAL_DX_LIST LIKE '%K51.914%'     THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.0%'   OR CS.FINAL_DX_LIST LIKE '%K57.0%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.12%'  OR CS.FINAL_DX_LIST LIKE '%K57.12%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.13%'  OR CS.FINAL_DX_LIST LIKE '%K57.13%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.2%'   OR CS.FINAL_DX_LIST LIKE '%K57.2%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.32%'  OR CS.FINAL_DX_LIST LIKE '%K57.32%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.33%'  OR CS.FINAL_DX_LIST LIKE '%K57.33%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.4%'   OR CS.FINAL_DX_LIST LIKE '%K57.4%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.52%'  OR CS.FINAL_DX_LIST LIKE '%K57.52%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.53%'  OR CS.FINAL_DX_LIST LIKE '%K57.53%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.8%'   OR CS.FINAL_DX_LIST LIKE '%K57.8%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.92%'  OR CS.FINAL_DX_LIST LIKE '%K57.92%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K57.93%'  OR CS.FINAL_DX_LIST LIKE '%K57.93%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K61%'     OR CS.FINAL_DX_LIST LIKE '%K61%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K63.0%'   OR CS.FINAL_DX_LIST LIKE '%K63.0%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K63.1%'   OR CS.FINAL_DX_LIST LIKE '%K63.1%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K65%'     OR CS.FINAL_DX_LIST LIKE '%K65%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K67%'     OR CS.FINAL_DX_LIST LIKE '%K67%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K68%'     OR CS.FINAL_DX_LIST LIKE '%K68%'         THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K75.0%'   OR CS.FINAL_DX_LIST LIKE '%K75.0%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K75.1%'   OR CS.FINAL_DX_LIST LIKE '%K75.1%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K81.0%'   OR CS.FINAL_DX_LIST LIKE '%K81.0%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K81.2%'   OR CS.FINAL_DX_LIST LIKE '%K81.2%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K82.2%'   OR CS.FINAL_DX_LIST LIKE '%K82.2%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K83.2%'   OR CS.FINAL_DX_LIST LIKE '%K83.2%'       THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.02%'  OR CS.FINAL_DX_LIST LIKE '%K85.02%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.12%'  OR CS.FINAL_DX_LIST LIKE '%K85.12%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.22%'  OR CS.FINAL_DX_LIST LIKE '%K85.22%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.32%'  OR CS.FINAL_DX_LIST LIKE '%K85.32%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.82%'  OR CS.FINAL_DX_LIST LIKE '%K85.82%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K85.92%'  OR CS.FINAL_DX_LIST LIKE '%K85.92%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K90.81%'  OR CS.FINAL_DX_LIST LIKE '%K90.81%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.02%'  OR CS.FINAL_DX_LIST LIKE '%K94.02%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.12%'  OR CS.FINAL_DX_LIST LIKE '%K94.12%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.22%'  OR CS.FINAL_DX_LIST LIKE '%K94.22%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K94.32%'  OR CS.FINAL_DX_LIST LIKE '%K94.32%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K95.01%'  OR CS.FINAL_DX_LIST LIKE '%K95.01%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%K95.81%'  OR CS.FINAL_DX_LIST LIKE '%K95.81%'      THEN 3    -- GASTROINTESTINAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.01%'  OR CS.FINAL_DX_LIST LIKE '%L02.01%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.11%'  OR CS.FINAL_DX_LIST LIKE '%L02.11%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.21%'  OR CS.FINAL_DX_LIST LIKE '%L02.21%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.31%'  OR CS.FINAL_DX_LIST LIKE '%L02.31%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.41%'  OR CS.FINAL_DX_LIST LIKE '%L02.41%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.51%'  OR CS.FINAL_DX_LIST LIKE '%L02.51%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.61%'  OR CS.FINAL_DX_LIST LIKE '%L02.61%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.81%'  OR CS.FINAL_DX_LIST LIKE '%L02.81%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L02.91%'  OR CS.FINAL_DX_LIST LIKE '%L02.91%'      THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L03%'     OR CS.FINAL_DX_LIST LIKE '%L03%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L04%'     OR CS.FINAL_DX_LIST LIKE '%L04%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L08%'     OR CS.FINAL_DX_LIST LIKE '%L08%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L88%'     OR CS.FINAL_DX_LIST LIKE '%L88%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L92.8%'   OR CS.FINAL_DX_LIST LIKE '%L92.8%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L98.0%'   OR CS.FINAL_DX_LIST LIKE '%L98.0%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%L98.3%'   OR CS.FINAL_DX_LIST LIKE '%L98.3%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M00%'     OR CS.FINAL_DX_LIST LIKE '%M00%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M01%'     OR CS.FINAL_DX_LIST LIKE '%M01%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M00%'     OR CS.FINAL_DX_LIST LIKE '%M00%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M46.2%'   OR CS.FINAL_DX_LIST LIKE '%M46.2%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M60%'     OR CS.FINAL_DX_LIST LIKE '%M60%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M71%'     OR CS.FINAL_DX_LIST LIKE '%M71%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M72.6%'   OR CS.FINAL_DX_LIST LIKE '%M72.6%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M72.8%'   OR CS.FINAL_DX_LIST LIKE '%M72.8%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M86%'     OR CS.FINAL_DX_LIST LIKE '%M86%'         THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M89.6%'   OR CS.FINAL_DX_LIST LIKE '%M89.6%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%M90.8%'   OR CS.FINAL_DX_LIST LIKE '%M90.8%'       THEN 4    -- SKIN/SOFT TISSUE
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N28.85%'  OR CS.FINAL_DX_LIST LIKE '%N28.85%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N28.86%'  OR CS.FINAL_DX_LIST LIKE '%N28.86%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N30%'     OR CS.FINAL_DX_LIST LIKE '%N30%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N34%'     OR CS.FINAL_DX_LIST LIKE '%N34%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N41%'     OR CS.FINAL_DX_LIST LIKE '%N41%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N43.1%'   OR CS.FINAL_DX_LIST LIKE '%N43.1%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N45%'     OR CS.FINAL_DX_LIST LIKE '%N45%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N48.2%'   OR CS.FINAL_DX_LIST LIKE '%N48.2%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N49%'     OR CS.FINAL_DX_LIST LIKE '%N49%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N51%'     OR CS.FINAL_DX_LIST LIKE '%N51%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N61.0%'   OR CS.FINAL_DX_LIST LIKE '%N61.0%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N61.1%'   OR CS.FINAL_DX_LIST LIKE '%N61.1%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N70%'     OR CS.FINAL_DX_LIST LIKE '%N70%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N71%'     OR CS.FINAL_DX_LIST LIKE '%N71%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N72%'     OR CS.FINAL_DX_LIST LIKE '%N72%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N73%'     OR CS.FINAL_DX_LIST LIKE '%N73%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N74%'     OR CS.FINAL_DX_LIST LIKE '%N74%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N75%'     OR CS.FINAL_DX_LIST LIKE '%N75%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N76%'     OR CS.FINAL_DX_LIST LIKE '%N76%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N77%'     OR CS.FINAL_DX_LIST LIKE '%N77%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%N98.0%'   OR CS.FINAL_DX_LIST LIKE '%N98.0%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.0%'   OR CS.FINAL_DX_LIST LIKE '%O03.0%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.37%'  OR CS.FINAL_DX_LIST LIKE '%O03.37%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.38%'  OR CS.FINAL_DX_LIST LIKE '%O03.38%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.5%'   OR CS.FINAL_DX_LIST LIKE '%O03.5%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.87%'  OR CS.FINAL_DX_LIST LIKE '%O03.87%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O03.88%'  OR CS.FINAL_DX_LIST LIKE '%O03.88%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O07.0%'   OR CS.FINAL_DX_LIST LIKE '%O07.0%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O07.37%'  OR CS.FINAL_DX_LIST LIKE '%O07.37%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O07.38%'  OR CS.FINAL_DX_LIST LIKE '%O07.38%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O04.5%'   OR CS.FINAL_DX_LIST LIKE '%O04.5%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O04.87%'  OR CS.FINAL_DX_LIST LIKE '%O04.87%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O04.88%'  OR CS.FINAL_DX_LIST LIKE '%O04.88%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O08.0%'   OR CS.FINAL_DX_LIST LIKE '%O08.0%'       THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O08.82%'  OR CS.FINAL_DX_LIST LIKE '%O08.82%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O08.83%'  OR CS.FINAL_DX_LIST LIKE '%O08.83%'      THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O23%'     OR CS.FINAL_DX_LIST LIKE '%O23%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O85%'     OR CS.FINAL_DX_LIST LIKE '%O85%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O86%'     OR CS.FINAL_DX_LIST LIKE '%O86%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%O91%'     OR CS.FINAL_DX_LIST LIKE '%O91%'         THEN 1    -- GENITOURINARY
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A00%'     OR CS.FINAL_DX_LIST LIKE '%A00%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A01%'     OR CS.FINAL_DX_LIST LIKE '%A01%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A04%'     OR CS.FINAL_DX_LIST LIKE '%A04%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A05%'     OR CS.FINAL_DX_LIST LIKE '%A05%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A19%'     OR CS.FINAL_DX_LIST LIKE '%A18%'         THEN 6    -- OTHER - BACTEREMIA
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A2%'      OR CS.FINAL_DX_LIST LIKE '%A2%'          THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A31%'     OR CS.FINAL_DX_LIST LIKE '%A31%'         THEN 4    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A32%'     OR CS.FINAL_DX_LIST LIKE '%A32%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A35%'     OR CS.FINAL_DX_LIST LIKE '%A35%'         THEN 6    -- OTHER - TETANUS
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A36.8%'   OR CS.FINAL_DX_LIST LIKE '%A36.8%'       THEN 6    -- OTHER - DIPHTHERIA
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A38%'     OR CS.FINAL_DX_LIST LIKE '%A38%'         THEN 6    -- OTHER - SCARLET FEVER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A42%'     OR CS.FINAL_DX_LIST LIKE '%A42%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A43%'     OR CS.FINAL_DX_LIST LIKE '%A43%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A48%'     OR CS.FINAL_DX_LIST LIKE '%A48%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A49%'     OR CS.FINAL_DX_LIST LIKE '%A49%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A50%'     OR CS.FINAL_DX_LIST LIKE '%A50%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A51%'     OR CS.FINAL_DX_LIST LIKE '%A51%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A52%'     OR CS.FINAL_DX_LIST LIKE '%A52%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A53%'     OR CS.FINAL_DX_LIST LIKE '%A53%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A56%'     OR CS.FINAL_DX_LIST LIKE '%A56%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A6%'      OR CS.FINAL_DX_LIST LIKE '%A6%'          THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B35%'     OR CS.FINAL_DX_LIST LIKE '%B35%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B36%'     OR CS.FINAL_DX_LIST LIKE '%B36%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B95%'     OR CS.FINAL_DX_LIST LIKE '%B95%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B96%'     OR CS.FINAL_DX_LIST LIKE '%B96%'         THEN 9    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%H32%'     OR CS.FINAL_DX_LIST LIKE '%H32%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%I30%'     OR CS.FINAL_DX_LIST LIKE '%I30%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%I32%'     OR CS.FINAL_DX_LIST LIKE '%I32%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%I33%'     OR CS.FINAL_DX_LIST LIKE '%I33%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%I38%'     OR CS.FINAL_DX_LIST LIKE '%I38%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%I39%'     OR CS.FINAL_DX_LIST LIKE '%I39%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%I40%'     OR CS.FINAL_DX_LIST LIKE '%I40%'         THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T80.2%'   OR CS.FINAL_DX_LIST LIKE '%T80.2%'       THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T81.4%'   OR CS.FINAL_DX_LIST LIKE '%T81.4%'       THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T82.6XXA%'OR CS.FINAL_DX_LIST LIKE '%T82.6XXA%'    THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T82.7XXA%'OR CS.FINAL_DX_LIST LIKE '%T82.7XXA%'    THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%T82.6XXA%'OR CS.FINAL_DX_LIST LIKE '%T82.6XXA%'    THEN 6    -- OTHER
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B37%'     OR CS.FINAL_DX_LIST LIKE '%B37%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B38%'     OR CS.FINAL_DX_LIST LIKE '%B38%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B39%'     OR CS.FINAL_DX_LIST LIKE '%B39%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B40%'     OR CS.FINAL_DX_LIST LIKE '%B40%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B41%'     OR CS.FINAL_DX_LIST LIKE '%B41%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B42%'     OR CS.FINAL_DX_LIST LIKE '%B42%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B43%'     OR CS.FINAL_DX_LIST LIKE '%B43%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B44%'     OR CS.FINAL_DX_LIST LIKE '%B44%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B45%'     OR CS.FINAL_DX_LIST LIKE '%B45%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B46%'     OR CS.FINAL_DX_LIST LIKE '%B46%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B47%'     OR CS.FINAL_DX_LIST LIKE '%B47%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B48%'     OR CS.FINAL_DX_LIST LIKE '%B48%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%B49%'     OR CS.FINAL_DX_LIST LIKE '%B49%'         THEN 9    -- FUNGAL
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A40%'     OR CS.FINAL_DX_LIST LIKE '%A40%'         THEN 8    -- SEPTICEMIA
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%A41%'     OR CS.FINAL_DX_LIST LIKE '%A41%'         THEN 8    -- SEPTICEMIA
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%R65%'     OR CS.FINAL_DX_LIST LIKE '%R65%'         THEN 8    -- SEPTICEMIA
       WHEN  MHX.MEDICAL_HX_LIST LIKE '%R78.81%'  OR CS.FINAL_DX_LIST LIKE '%R78.81%'      THEN 8    -- SEPTICEMIA
             ELSE 0 -- Evaluates for the presence of SITE OF INFECTION
  END AS C_SITE_OF_INFECTION
      
	  
/* ------------------------------------------------------------------------
! CLINICAL OUTCOME VARIABLES
!------------------------------------------------------------------------- */

, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%I82.4%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%I26.0%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%I26.9%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%O08.2%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%O88.2%'       THEN 1    -- PE
       WHEN CS.FINAL_DX_LIST LIKE '%O88.3%'       THEN 1    -- PE
             ELSE 0 -- Evaluates for the presence of PE/DVT
  END AS O_PE_DVT_ACUTE

, CASE
       WHEN CS.FINAL_DX_LIST LIKE '%Z93%'         THEN 1    -- PE
       WHEN  PCS.PX_LIST LIKE '%0B110F4%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0B113F4%'         THEN 1    -- 
       WHEN  PCS.PX_LIST LIKE '%0B114F4%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACUTE TRACHEOSTOMY
  END AS O_TRACHEOSTOMY

, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%I20%'         THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I21%'         THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I22%'         THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.110%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.700%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.710%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.720%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.730%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.750%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.760%'     THEN 1    -- 
       WHEN CS.FINAL_DX_LIST LIKE '%I25.790%'     THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_CARDIAC_ACS
  
, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%I63%'         THEN 1    -- 
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_CARDIAC_CVA
  
, CASE 
       WHEN CS.FINAL_DX_LIST LIKE '%B33.22%'      THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%I40%'         THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%I41%'         THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%I51.4%'       THEN 1    --
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_CARDIAC_MYOCARDITIS

, CASE
       WHEN CS.FINAL_DX_LIST LIKE '%O98.513%'     THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%O98.519%'     THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%O98.52%'      THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%O98.53%'      THEN 1    --
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_PREGNANCY_VIRAL

, CASE
       WHEN CS.FINAL_DX_LIST LIKE '%T80.211%'     THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%T80.212%'     THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%T80.218%'     THEN 1    --
       WHEN CS.FINAL_DX_LIST LIKE '%T80.219%'     THEN 1    --
             ELSE 0 -- Evaluates for the presence of ACS
  END AS O_HAI_CVC


FROM
BasePop BP
INNER JOIN ICD_10_CM_LIST CS ON BP.PAT_ENC_CSN_ID = CS.PAT_ENC_CSN_ID
LEFT JOIN MEDICAL_HX_LIST MHX ON CS.PAT_ENC_CSN_ID = MHX.PAT_ENC_CSN_ID
LEFT JOIN ICD_10_PCS_LIST PCS ON CS.PAT_ENC_CSN_ID = PCS.PAT_ENC_CSN_ID
)

/* ------------------------------------------------------------------------
! Ventilator Data is captured in the F_VENT_EPISODES table.  Institutions 
! should validate that this table accurately represents ventilator use and 
! initiation.  Utilizing this table should allow consistent capture of 
! ventilator start/end times.
!------------------------------------------------------------------------- */

, VENT_EPISODES AS (
SELECT
BP.HSP_ACCOUNT_ID
, BP.INPATIENT_DATA_ID
, V.PAT_ID
, V.VENT_START_DTTM AS VentilatorStartDatetime
, V.VENT_END_DTTM AS VentilatorEndDatetime

FROM
BasePop BP
INNER JOIN (SELECT INPATIENT_DATA_ID, PAT_ID, VENT_START_DTTM, VENT_END_DTTM, 
                   ROW_NUMBER() OVER (PARTITION BY INPATIENT_DATA_ID, VENT_START_DTTM ORDER BY INPATIENT_DATA_ID)
				   AS VENT_RN FROM F_VENT_EPISODES)V ON BP.INPATIENT_DATA_ID = V.INPATIENT_DATA_ID AND VENT_RN = 1
)


select 
      UNIQUE_PATIENT_IDENTIFIER
      , PATIENT_CONTROL_NUMBER
      , S.DATE_OF_BIRTH
      , MARITAL_STATUS
      , S.SEX
      , GENDER_IDENTITY
      , SEXUAL_ORIENTATION
      , s.age
      , S.RACE
      , S.RACE_DESC
      , S.ETHNICITY
      , S.ETHNICITY_DESC
      , S.PAYER
      , S.FINANCIAL_CLASS_DESC
      , S.INSURANCE_NUMBER
      , s.pat_mrn_id AS MEDICAL_RECORD_NUMBER
      , S.FACILITY_IDENTIFIER
      , S.ADMISSION_DATETIME
      , S.SOURCE_OF_ADMISSION
      , S.SOURCE_FACILITY
      , S.DISCHARGE_DATETIME
      , S.DISCH_DISP_C
      , S.ADM_PROV_ID
      , AA.PROV_NAME AS ADM_PROV_NAME
      , S.ADM_ATND_PROV_ID
      , AD.PROV_NAME AS ADM_ATTEND_NAME
      , S.DISCH_ATND_PROV_ID
      , DD.PROV_NAME AS DISCH_ATTEND_NAME
      , E.CURRENT_ICD10_LIST AS PRIMARY_DIAGNOSIS
      , E.DX_NAME AS PRIMARY_DIAGNOSIS_DESC
      , CASE WHEN S.DISCH_DISP_C = 20 THEN 'Y'
             WHEN S.DISCH_DISP_C = 40 THEN 'Y'
             ELSE 'N' END AS MORT
      , CASE WHEN S.FACILITY_IDENTIFIER = 635 THEN 'UH'
             WHEN S.FACILITY_IDENTIFIER = 628 THEN 'CC' END AS CAMPUS
      , S.DISCH_DISP_C AS DISCHARGE_DISP
      , DP.NAME AS DISCHARGE_DISP_DESC
      , S.PAT_LAST_NAME
      , S.PAT_FIRST_NAME
      , FINAL_DX_LIST
      , FINAL_DX_NAMES
      , MEDICAL_HX_LIST
      , PX_LIST
      , CPT_LIST
      , HX_AIDS_HIV_DISEASE
      , HX_ASTHMA
      , HX_CHRONIC_LIVER_DISEASE
      , HX_CHRONIC_RENAL_DISEASE 
      , HX_CKD_STAGE
      , HX_CHRONIC_RESPIRATORY_FAILURE
      , HX_CONGESTIVE_HEART_FAILURE
      , HX_COPD
      , HX_DEMENTIA
      , HX_DIABETES_I
      , HX_DIABETES_II
      , HX_DIABETES_MIXED
      , HX_DIALYSIS
      , HX_HYPERTENSION
      , HX_IMMUNOCOMPROMISED
      , HX_LYMPHOMA_LEUKEMIA_MULTI_MYELOMA
      , HX_METASTATIC_CANCER
      , HX_OBESITY
      , HX_PREGNANCY
      , HX_PREGNANCY_DURING_HOSPITALIZATION_SEPSIS
      , HX_PREGNANCY_DURING_HOSPITALIZATION_COVID
      , HX_SMOKING_VAPING
      , HX_TRACHEOSTOMY_ARRIVAL
      , HX_TRANSPLANT
      , CC_ACUTE_CARDIOVASCULAR
      , CC_ALTERED_MENTAL_STATUS
      , CC_COAGULOPATHY
      , CC_ACUTE_HYPOXIC_RESP_FAILURE
      , CC_ACUTE_RESP_FAILURE
      , CC_ARDS
      , CC_PEDS_MISC
      , CC_ACUTE_LIVER_INJURY
      , CC_BACTEREMIA
      , CC_ACUTE_RENAL_INJURY_N_HEMODIALYSIS
      , CC_ACUTE_RENAL_FAILURE_R_HEMODIALYSIS
      , CC_BACTERIAL_PNEUMONIA
      , CC_CARDIAC_ARREST
      , CC_RESPIRATORY_ARREST
      , CC_ATRIAL_FIB_NEW
      , CC_HEART_BLOCK_NEW
      
      , T_DIALYSIS
      , T_DIALYSIS_DATE
      , T_ECMO
      , T_ECMO_DATE
      , T_MECHANICAL_VENTILATION
      , T_MECHANICAL_VENTILATION_DATE
      , T_NI_VENTILATION
      , T_NI_VENTILATION_DATE
      , T_REMDESIVIR
      , T_REMDESIVIR_DATE
      , T_SARILUMAB
      , T_SARILUMAB_DATE
      , T_TOCILIZUMAB
      , T_TOCILIZUMAB_DATE
      , T_CON_PLASMA
      , T_CON_PLASMA_DATE
      , T_NOVEL_TREATMENTS
      , T_NOVEL_TREATMENTS_DATE
      , T_ALBUMIN
      , T_ALBUMIN_DATE
      , T_CRYO
      , T_CRYO_DATE
      , T_FFP
      , T_FFP_DATE
      , T_PLATELETS
      , T_PLATELETS_DATE
      , T_THROMBOLYTIC
      , T_THROMBOLYTIC_DATE
      , T_VASOPRESSOR
      , T_VASOPRESSOR_DATE
      , T_SEDATIVES
      , T_SEDATIVES_DATE
      , T_CHEMO
      , T_CENTRAL_ACCESS
      , T_CENTRAL_ACCESS_DATE
      , C_COVID_VIRUS
      , C_COVID_EXPOSURE
      , C_DRUG_RESISTANT_PATHOGEN
      , C_SITE_OF_INFECTION
      , O_PE_DVT_ACUTE
      , O_TRACHEOSTOMY
      , O_CARDIAC_ACS
      , O_CARDIAC_CVA
      , O_CARDIAC_MYOCARDITIS
      , O_PREGNANCY_VIRAL
      , O_HAI_CVC
	  , VentilatorStartDatetime
	  , VentilatorEndDatetime
	  , CASE WHEN VENTILATORSTARTDATETIME IS NOT NULL AND VENTILATORENDDATETIME IS NOT NULL THEN 
	              TRUNC(VENTILATORENDDATETIME, 'DD') - TRUNC(VENTILATORSTARTDATETIME, 'DD') 
				  ELSE NULL 
		END AS VentilatorDays
  from
(select bp.pat_enc_csn_id
      ,bp.hsp_account_id AS PATIENT_CONTROL_NUMBER
      ,bp.pat_mrn_id
      , BP.PAT_NAME
      , BP.PAT_LAST_NAME
      , BP.PAT_FIRST_NAME
      , TO_CHAR(BP.BIRTH_DATE, 'YYYY-MM-DD') AS DATE_OF_BIRTH
      , bp.age
      , MARITAL_STATUS
      , BP.SEX
      , GENDER_IDENTITY
      , SEXUAL_ORIENTATION
      , ( substr(BP.pat_last_name,1,2) || SUBSTR(BP.PAT_LAST_NAME,-2,2) || SUBSTR(BP.PAT_FIRST_NAME,1,2) || SUBSTR(BP.SSN,-4,4) ) AS UNIQUE_PATIENT_IDENTIFIER
      , BP.FACILITY_IDENTIFIER
      , BP.HOSP_DISCH_TIME AS DISCHARGE_DATETIME
      , BP.DISCH_DISP_C
      , BP.RACE
      , BP.RACE_DESC
      , BP.ETHNICITY
      , BP.ETHNICITY_DESC
      , BP.PAYER
      , BP.FINANCIAL_CLASS_DESC
      , BP.INSURANCE_NUMBER
      , BP.HOSP_ADMSN_TIME AS ADMISSION_DATETIME
      , BP.SOURCE_OF_ADMISSION
      , BP.SOURCE_FACILITY
      , BP.ADM_PROV_ID
      , BP.ADM_ATND_PROV_ID
      , BP.DISCH_PROV_ID
      , BP.DISCH_ATND_PROV_ID
      , CM.FINAL_DX_LIST 
      , CM.FINAL_DX_NAMES
      , PX_LIST
      , CPT_LIST
      , MHX.MEDICAL_HX_LIST
      , HX_AIDS_HIV_DISEASE
      , HX_ASTHMA
      , HX_CHRONIC_LIVER_DISEASE
      , HX_CHRONIC_RENAL_DISEASE 
      , HX_CKD_STAGE
      , HX_CHRONIC_RESPIRATORY_FAILURE
      , HX_CONGESTIVE_HEART_FAILURE
      , HX_COPD
      , HX_DEMENTIA
      , HX_DIABETES_I
      , HX_DIABETES_II
      , HX_DIABETES_MIXED
      , HX_DIALYSIS
      , HX_HYPERTENSION
      , HX_IMMUNOCOMPROMISED
      , HX_LYMPHOMA_LEUKEMIA_MULTI_MYELOMA
      , HX_METASTATIC_CANCER
      , HX_OBESITY
      , HX_PREGNANCY
      , HX_PREGNANCY_DURING_HOSPITALIZATION_SEPSIS
      , HX_PREGNANCY_DURING_HOSPITALIZATION_COVID
      , HX_SMOKING_VAPING
      , HX_TRACHEOSTOMY_ARRIVAL
      , HX_TRANSPLANT
      , CC_ACUTE_CARDIOVASCULAR
      , CC_ALTERED_MENTAL_STATUS
      , CC_COAGULOPATHY
      , CC_ACUTE_HYPOXIC_RESP_FAILURE
      , CC_ACUTE_RESP_FAILURE
      , CC_ARDS
      , CC_PEDS_MISC
      , CC_ACUTE_LIVER_INJURY
      , CC_BACTEREMIA
      , CC_ACUTE_RENAL_INJURY_N_HEMODIALYSIS
      , CC_ACUTE_RENAL_FAILURE_R_HEMODIALYSIS
      , CC_BACTERIAL_PNEUMONIA
      , CC_CARDIAC_ARREST
      , CC_RESPIRATORY_ARREST
      , CC_ATRIAL_FIB_NEW
      , CC_HEART_BLOCK_NEW
      
      , T_DIALYSIS
      , T_DIALYSIS_DATE
      , T_ECMO
      , T_ECMO_DATE
      , T_MECHANICAL_VENTILATION
      , T_MECHANICAL_VENTILATION_DATE
      , T_NI_VENTILATION
      , T_NI_VENTILATION_DATE
      , T_REMDESIVIR
      , T_REMDESIVIR_DATE
      , T_SARILUMAB
      , T_SARILUMAB_DATE
      , T_TOCILIZUMAB
      , T_TOCILIZUMAB_DATE
      , T_CON_PLASMA
      , T_CON_PLASMA_DATE
      , T_NOVEL_TREATMENTS
      , T_NOVEL_TREATMENTS_DATE
      , T_ALBUMIN
      , T_ALBUMIN_DATE
      , T_CRYO
      , T_CRYO_DATE
      , T_FFP
      , T_FFP_DATE
      , T_PLATELETS
      , T_PLATELETS_DATE
      , T_THROMBOLYTIC
      , T_THROMBOLYTIC_DATE
      , T_VASOPRESSOR
      , T_VASOPRESSOR_DATE
      , T_SEDATIVES
      , T_SEDATIVES_DATE
      , T_CHEMO
      , T_CENTRAL_ACCESS
      , T_CENTRAL_ACCESS_DATE
      , C_COVID_VIRUS
      , C_COVID_EXPOSURE
      , C_DRUG_RESISTANT_PATHOGEN
      , C_SITE_OF_INFECTION
      , O_PE_DVT_ACUTE
      , O_TRACHEOSTOMY
      , O_CARDIAC_ACS
      , O_CARDIAC_CVA
      , O_CARDIAC_MYOCARDITIS
      , O_PREGNANCY_VIRAL
      , O_HAI_CVC
	  , V.VentilatorStartDatetime
	  , V.VentilatorEndDatetime
      
      , ROW_NUMBER() OVER (PARTITION BY bp.PAT_ENC_CSN_ID ORDER BY bp.PAT_ENC_CSN_ID) SEQ_NO
   from BasePop bp
LEFT outer JOIN ICD_10_CM_LIST           CM ON bp.PAT_ENC_CSN_ID = CM.PAT_ENC_CSN_ID
LEFT outer JOIN MEDICAL_HX_LIST         MHX ON bp.PAT_ENC_CSN_ID = MHX.PAT_ENC_CSN_ID
LEFT outer JOIN CC_MCCS_MAPPED           CC ON bp.PAT_ENC_CSN_ID = CC.PAT_ENC_CSN_ID
LEFT OUTER JOIN ICD_10_PCS_LIST         PCS ON BP.PAT_ENC_CSN_ID = PCS.PAT_ENC_CSN_ID
LEFT OUTER JOIN CPT_LIST                CPT ON BP.PAT_ENC_CSN_ID = CPT.PAT_ENC_CSN_ID
LEFT OUTER JOIN VENT_EPISODES             V ON BP.INPATIENT_DATA_ID = V.INPATIENT_DATA_ID



   WHERE
        (
          CM.FINAL_DX_LIST IS NOT NULL
       )

  order BY
        bp.PAT_ENC_CSN_ID
       ,bp.HSP_ACCOUNT_ID
       ,bp.PAT_MRN_ID
       ,bp.age

     )S
LEFT OUTER JOIN CLARITY_SER              AA ON S.ADM_PROV_ID = AA.PROV_ID
LEFT OUTER JOIN CLARITY_SER              AD ON S.ADM_ATND_PROV_ID = AD.PROV_ID
LEFT OUTER JOIN CLARITY_SER              DD ON S.DISCH_ATND_PROV_ID = DD.PROV_ID
INNER JOIN HSP_ACCT_DX_LIST              DX ON S.PATIENT_CONTROL_NUMBER = DX.HSP_ACCOUNT_ID AND LINE = 1
INNER JOIN CLARITY_EDG                    E ON DX.DX_ID = E.DX_ID
INNER JOIN ZC_DISCH_DISP                 DP ON S.DISCH_DISP_C = DP.DISCH_DISP_C
WHERE SEQ_NO = 1  