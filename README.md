# NYS SEPSIS COVID DATA COLLECTION

![Bitbucket Issues](https://img.shields.io/bitbucket/issues-raw/nysdoh/sepsis-covid-data-collection)

![Bitbucket Pull Requests](http://img.shields.io/bitbucket/pr-raw/nysdoh/sepsis-covid-data-collection)

The NYS Sepsis Covid Data Collection Project(NYSSCDC) is a collaborative peer sharing excercise to reduce burden abstracting data from EHRs for reporting to the NYS DOH Sepsis portal. 

IPRO publishes a Sepsis Data Dictionary which outlines the data elements and related constraints for those elements. Reporting entities are free to submit SQL for any EHR system, for any version, fo ruse by others in kickstarting their abstraction process.

## Using NYSSCDC

Each system directory houses the files and code needed for each dictionary version.

## Contributing to NYSSCDC

To contribute to NYSSCDC, follow these steps:

1. Fork this repository.

2. Create a branch: git checkout -b <branch_name>.

3. Make your changes and commit them: git commit -m '<commit_message>'

4. Push to the original branch: git push origin <project_name>/<location>

5. Create the pull request.

Alternatively see the documentation for [creating a pull request](https://bitbucket.org/nysdoh/sepsis-covid-data-collection/wiki/Contributing%20Guide).

Contributions should match the relevant data dictionary, therefore shared assets for Sepsis Data Dictionary 1.1 must be shared to the relevant EHR 1.1 folder.

All contributions must state the EHR system, version, and backend (eg Oracle, MSSQL) the code is intended to be used with, for example:

* vendor_system_version_rdbms_dictionary.number

* allscripts_sunrise_14_oracle_1.1

## Feature List

To maintain consistency across the build, we need to create a list of features which we need to complete so that the data pulls will meet the requirements of the current Data Dictionary. Once the Data Dictionary is uploaded and available for viewing, we need to create a list of those features, and then monitor them in the ## Issues area.

## Contributors

Thanks to the following people who have contributed to this project:

* @collin_york

* @G. Briddick

## Contact

If you want to contact support you can reach me at <support@ipro.us>.

* A Google Group is available at https://groups.google.com/a/cloud.ipro.org/g/ny-sepsis-ehr-discuss

* Members of the Group can join Chat at https://chat.google.com/room/AAAAgGeY1v4

* If you want to contact support, or request a membership invitation, contact IPRO at <support@ipro.us>.

## License

This project uses the following license: [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode). Contributions are intended to be freely shared under this non-commercial license.